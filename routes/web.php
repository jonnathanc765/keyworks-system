<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Auth::routes();

// Cancel register Roots
Route::get('register', function ()
{
    return redirect()->route('login');
})->name('register');

Route::post('register', function ()
{
    return redirect()->route('login');
});

Route::get('/', function ()
{
    return redirect()->route('admin.index');
});


Route::group(['middleware' => ['auth']], function () {


    require __DIR__.'/admin.php';


    Route::get('/config', 'ConfigController@index')->name('config.index');
    Route::put('/config', 'ConfigController@update')->name('config.update');


    Route::prefix('freepasses')->group(function ()
    {

        Route::get('/{user}/{user_type}', 'Backend\FreepassController@index')->name('freepasses.index');
        Route::post('/{user}/{user_type}/send', 'Backend\FreepassController@send')->name('freepasses.send');
        Route::post('/{user}/{user_type}/{freepass}', 'Backend\FreepassController@cancel')->name('freepasses.cancel');

    });



});

Route::get('/migrate', function ()
{
    Artisan::call('migrate');

    return 'Migrado Exitoso';
});

Route::get('/config-clear', function ()
{
    Artisan::call('config:clear');
    Artisan::call('cache:clear');

    return 'Memoria cache limpiada';
});
