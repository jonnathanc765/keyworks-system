<?php


    Route::prefix('admin')->group(function ()
    {

        Route::get('/', 'Backend\AdminController@index')->name('admin.index');

        Route::prefix('contact')->group(function ()
        {
            Route::get('/', 'Backend\ContactController@index')->name('admin.contact.index');
        });

        Route::prefix('ambassador')->group(function ()
        {
            Route::get('/', 'Backend\AmbassadorController@index')->name('admin.ambassadors.index');
            Route::get('/create', 'Backend\AmbassadorController@create')->name('admin.ambassadors.create');
            Route::post('/store', 'Backend\AmbassadorController@store')->name('admin.ambassadors.store');
            Route::get('/show/{ambassador}', 'Backend\AmbassadorController@show')->name('admin.ambassadors.show');
            Route::put('/update/{ambassador}', 'Backend\AmbassadorController@update')->name('admin.ambassadors.update');
            Route::delete('/destroy/{ambassador}', 'Backend\AmbassadorController@destroy')->name('admin.ambassadors.destroy');
            Route::post('/code', 'Backend\AmbassadorController@code')->name('admin.ambassadors.code');
        });

        Route::prefix('keyworker')->group(function ()
        {
            Route::get('/', 'Backend\KeyworkerController@index')->name('admin.keyworkers.index');
            Route::get('/create', 'Backend\KeyworkerController@create')->name('admin.keyworkers.create');
            Route::post('/store', 'Backend\KeyworkerController@store')->name('admin.keyworkers.store');
            Route::get('/show/{user}', 'Backend\KeyworkerController@show')->name('admin.keyworkers.show');
            Route::put('/update/{user}', 'Backend\KeyworkerController@update')->name('admin.keyworkers.update');
            Route::delete('/destroy/{user}', 'Backend\KeyworkerController@destroy')->name('admin.keyworkers.destroy');
        });

        Route::prefix('plans')->group(function ()
        {
            Route::get('/', 'Backend\PlanController@index')->name('admin.plans.index');
            Route::get('/create', 'Backend\PlanController@create')->name('admin.plans.create');
            Route::post('/store', 'Backend\PlanController@store')->name('admin.plans.store');
            Route::get('/show/{plan}', 'Backend\PlanController@show')->name('admin.plans.show');
            Route::put('/update/{plan}', 'Backend\PlanController@update')->name('admin.plans.update');
            Route::delete('/destroy/{plan}', 'Backend\PlanController@destroy')->name('admin.plans.destroy');
        });

        Route::prefix('payments')->group(function ()
        {

            Route::get('/create/{planUser}', 'Backend\PaymentController@create')->name('admin.payments.create');
            Route::post('/store/{planUser}', 'Backend\PaymentController@store')->name('admin.payments.store');

        });

        Route::prefix('organizations')->group(function ()
        {
            Route::post('/store', 'Backend\OrganizationController@store')->name('admin.organizations.store');
        });

        Route::prefix('freepasses')->group(function ()
        {

            Route::get('/create/{user}/{user_type}', 'Backend\FreepassController@create')->name('admin.freepasses.create');
            Route::post('/store/{user}/{user_type}', 'Backend\FreepassController@store')->name('admin.freepasses.store');

            Route::prefix('redeem')->group(function ()
            {

                Route::get('/create', 'Backend\RedeemController@create')->name('admin.freepasses.redeem.create');
                Route::post('/store', 'Backend\RedeemController@store')->name('admin.freepasses.redeem.store');
                Route::get('/success', 'Backend\RedeemController@success')->name('admin.freepasses.redeem.success');

            });

        });
        Route::prefix('renews')->group(function ()
        {
            Route::get('create/{user}', 'Backend\RenewController@create')->name('admin.renews.create');
            Route::post('store/{user}', 'Backend\RenewController@store')->name('admin.renews.store');
        });
        Route::prefix('visits')->group(function ()
        {

            Route::get('/create/{user?}', 'Backend\VisitController@create')->name('admin.visits.create');
            Route::post('/store', 'Backend\VisitController@store')->name('admin.visits.store');
            Route::get('/show/{planUser}', 'Backend\VisitController@show')->name('admin.visits.show');
            Route::delete('/destroy/{visit}', 'Backend\VisitController@destroy')->name('admin.visits.destroy');

        });
    });
