<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Deffer;
use Faker\Generator as Faker;

$factory->define(Deffer::class, function (Faker $faker) {
    return [
        'discount' => rand(1,10)
    ];
});
