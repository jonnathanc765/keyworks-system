<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ambassador;
use Faker\Generator as Faker;

$factory->define(Ambassador::class, function (Faker $faker) {
    return [
        'user_id' => null,
        'ambassador_code' => null,
        'instagram_account' => $faker->word,
        'another_social_account' => $faker->word,
        'phone' => $faker->numberBetween(9999, 9999999),
        'accumulated_amount' => rand(0, 100),
        'paid_amount' => rand(0, 500),
    ];
});
