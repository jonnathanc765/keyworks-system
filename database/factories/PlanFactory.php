<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name'              => $faker->word,
        'description'       => $faker->sentence,
        'ambassador_charge' => rand(5,10),
        'keyworker_charge'  => rand(5,10),
        'days'              => rand(1, 20),
        'is_monthly'        => false,
        'price'             => rand(10, 70)
    ];
});
