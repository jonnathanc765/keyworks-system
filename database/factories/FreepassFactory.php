<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Freepass;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Freepass::class, function (Faker $faker) {
    return [
        'code' => rand(1, 100),
        'status' => 'ACTIVO',
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'expiration_date' => Carbon::now()->add(rand(1,2), 'months')->add(rand(1,30), 'days'),
    ];
});
