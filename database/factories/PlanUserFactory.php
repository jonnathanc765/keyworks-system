<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PlanUser;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(PlanUser::class, function (Faker $faker) {
    return [
        'user_id' => null,
        'plan_name' => $faker->sentence,
        'notes' => $faker->sentence,
        'is_monthly' => rand(0,1),
        'amount' => $faker->randomElement([10, 30, 60]),
        'expiration_date' => Carbon::now(),
        'days' => rand(0,10)
    ];
});
