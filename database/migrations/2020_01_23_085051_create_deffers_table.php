<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deffers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('ambassador_code')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->float('discount')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('ambassador_code')->references('ambassador_code')->on('ambassadors')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deffers');
    }
}
