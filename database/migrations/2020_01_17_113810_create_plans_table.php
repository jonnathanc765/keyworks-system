<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->float('ambassador_charge')->default(0)->nullable();
            $table->float('keyworker_charge')->default(0)->nullable();
            $table->boolean('is_monthly')->default(false);
            $table->unsignedInteger('days')->nullable();
            $table->unsignedInteger('included_freepasses')->default(0);
            $table->float('price')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
