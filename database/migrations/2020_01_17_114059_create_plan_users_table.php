<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->string('plan_name');
            $table->text('notes')->nullable();
            $table->date('expiration_date')->nullable();
            $table->float('amount')->nullable();

            $table->boolean('is_monthly')->default(false);
            $table->unsignedInteger('days')->nullable();

            $table->boolean('is_paid')->default(false);
            $table->boolean('is_fixed')->default(false);
            $table->string('pay_method')->nullable();
            $table->date('paid_date')->nullable();
            $table->string('paid_ref')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_users');
    }
}
