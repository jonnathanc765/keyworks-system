<?php

use App\Ambassador;
use App\Plan;
use App\PlanUser;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Ambassador::truncate();

        $admin = factory(User::class)->create([
            'name' => 'John Doe',
            'password' => bcrypt('cheche123'),
            'email' => 'admin@gmail.com'
        ])->assignRole('super-admin');

        factory(User::class, 20)->create()->each(function ($user)
        {
            $ambassador = factory(Ambassador::class)->create([
                'user_id' => $user->id
            ]);
            $user->assignRole('ambassador');

            $code = explode(' ', trim($user->name));
            $code = strtolower($code[0]);

            $ambassador->update([
                'ambassador_code' => $code . '-' . $ambassador->id
            ]);
        });

        factory(User::class, 20)->create()->each(function ($user)
        {
            $ambassador = factory(Ambassador::class)->create([
                'user_id' => $user->id
            ]);
            $user->assignRole('ambassador');
            $user->assignRole('keyworker');

            factory(PlanUser::class)->create([
                'user_id' => $user->id
            ]);

            $code = explode(' ', trim($user->name));
            $code = strtolower($code[0]);

            $ambassador->update([
                'ambassador_code' => $code . '-' . $ambassador->id
            ]);

        });

        factory(User::class, 20)->create()->each(function ($user)
        {
            $user->assignRole('keyworker');
            factory(PlanUser::class)->create([
                'user_id' => $user->id
            ]);
        });



    }
}
