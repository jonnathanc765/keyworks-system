<?php

use Illuminate\Database\Seeder;

class ProductionPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Plan::truncate();
        App\Organization::truncate();

        App\Plan::create([
            'name' => 'Plan Control',
            'description' => 'Acceso a todos los espacios y servicios Café e hidratación gratuita Upgrade un espacio fijo por $10 mensuales Pase libre a actividades diarias gratuitas 3 pases GRATUITOS de un día (Plan F1)',
            'price' => 60,
            'keyworker_charge' => 10,
            'ambassador_charge' => 10,
            'included_freepasses' => 2,
            'days' => 0,
            'is_monthly' => true
        ]);
        App\Plan::create([
            'name' => 'Plan F10',
            'description' => 'Acceso a todos los espacios y servicios durante 10 dias al mes Café e hidratación gratuita Pase libre a actividades diarias gratuitas 1 pase GRATUITO de un dia (Plan F1)',
            'price' => 30,
            'keyworker_charge' => 0,
            'ambassador_charge' => 0,
            'included_freepasses' => 0,
            'days' => 10,
            'is_monthly' => false
        ]);
        App\Plan::create([
            'name' => 'Plan F1',
            'description' => 'Acceso a todos los espacios Café e hidratación incluida Pase libre a actividades del día',
            'price' => 10,
            'keyworker_charge' => 0,
            'ambassador_charge' => 0,
            'included_freepasses' => 0,
            'days' => 1,
            'is_monthly' => false
        ]);


    }
}
