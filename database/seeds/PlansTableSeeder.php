<?php

use App\Organization;
use App\Plan;
use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::truncate();
        Organization::truncate();

        factory(Plan::class, 5)->create();
        factory(Organization::class, 5)->create();




    }
}
