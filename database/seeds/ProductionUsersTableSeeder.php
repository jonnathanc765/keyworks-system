<?php

use App\User;
use Illuminate\Database\Seeder;

class ProductionUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $admin = User::create([
            'email' => 'admin@ikeyworks.com',
            'name' => 'Super Admin User'
        ]);

        $admin->assignRole('super-admin');
        $admin->assignRole('admin');

        $carlos = User::create([
            'email' => 'carlos@ikeyworks.com',
            'name' => 'Carlos castañeda'
        ]);

        $carlos->assignRole('admin');

        $ana = User::create([
            'email' => 'ana@ikeyworks.com',
            'name' => 'Ana Pereza'
        ]);

        $ana->assignRole('admin');

        $jonnathan = User::create([
            'email' => 'jonnathan@ikeyworks.com',
            'name' => 'Jonnathan Carrasco'
        ]);

        $jonnathan->assignRole('admin');

    }
}
