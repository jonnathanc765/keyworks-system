<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Role::truncate();
        Permission::truncate();

        // create permissions
        Permission::create(['name' => 'create keyworker']);
        Permission::create(['name' => 'update keyworker']);
        Permission::create(['name' => 'delete keyworker']);
        Permission::create(['name' => 'read keyworker']);

        Permission::create(['name' => 'create ambassador']);
        Permission::create(['name' => 'update ambassador']);
        Permission::create(['name' => 'delete ambassador']);
        Permission::create(['name' => 'read ambassador']);

        Permission::create(['name' => 'create hostess']);
        Permission::create(['name' => 'update hostess']);
        Permission::create(['name' => 'delete hostess']);
        Permission::create(['name' => 'read hostess']);

        Permission::create(['name' => 'create admin']);
        Permission::create(['name' => 'update admin']);
        Permission::create(['name' => 'delete admin']);
        Permission::create(['name' => 'read admin']);

        // create roles and assign created permissions
        Role::create(['name' => 'admin'])->givePermissionTo(
            [
                'create keyworker',
                'update keyworker',
                'delete keyworker',
                'read keyworker',

                'create ambassador',
                'update ambassador',
                'delete ambassador',
                'read ambassador',

                'create hostess',
                'update hostess',
                'delete hostess',
                'read hostess'
            ]
        );
        Role::create(['name' => 'ambassador']);
        Role::create(['name' => 'keyworker']);
        Role::create(['name' => 'hostess']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());




    }
}
