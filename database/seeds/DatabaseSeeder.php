<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        if (App::environment('local')) {

            $this->call(ContactsTableSeeder::class);
            $this->call(RolesAndPermissionsSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(PlansTableSeeder::class);

        } else {

            $this->call(RolesAndPermissionsSeeder::class);
            $this->call(ProductionUsersTableSeeder::class);
            $this->call(ProductionPlansTableSeeder::class);

        }


        Schema::enableForeignKeyConstraints();
    }
}
