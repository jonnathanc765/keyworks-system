@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="my-2">Lista de mis freepasses</h2>
                    <a href="{{ route('admin.freepasses.create', [$user, 'Admin']) }}" class="btn btn-success btn-lg float-right">Generar freepasses</a>
                    <p>Freepasses activos: {{ $user->activeFreepassesType('Admin')->count() }}</p>


                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">Todos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="active-tab" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="false">Activos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="redeemed-tab" data-toggle="tab" href="#redeemed" role="tab" aria-controls="redeemed" aria-selected="false">Canjeados</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="expired-tab" data-toggle="tab" href="#expired" role="tab" aria-controls="expired" aria-selected="false">Vencidos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="cancelled-tab" data-toggle="tab" href="#cancelled" role="tab" aria-controls="cancelled" aria-selected="false">Cancelados</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        {{-- Lista de freepasses (Todos)  --}}
                        <div class="tab-pane fade" id="all" role="tabpanel" aria-labelledby="all-tab">

                            <h2 class="text-center my-3">
                                Lista de <span class="text-primary"><strong>Todos</strong></span> los freepasses
                            </h2>
                            <table class="table dataTable text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Fecha de vencimiento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($all as $freepass)
                                    <tr>
                                        <td>{{ $freepass->code }}</td>
                                        <td>{{ $freepass->status }}</td>
                                        <td>{{ $freepass->expiration_date }}</td>
                                        <td>

                                        </td>
                                    </tr>
                                    @empty
                                        <h3>No tiene freepasses registrados</h3>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                        {{-- Lista de freepasses (Activos)  --}}
                        <div class="tab-pane fade show active" id="active" role="tabpanel" aria-labelledby="active-tab">

                            <h2 class="text-center my-3">
                                Lista de freepasses <span class="text-info"><strong>Activos</strong></span>
                            </h2>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <table class="table dataTable text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Sending</th>
                                        <th scope="col">Fecha de vencimiento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($active as $freepass)
                                    <tr>
                                        <td>
                                            {{ $freepass->code ? $freepass->code : $freepass->physical_code }}
                                            @if ($freepass->physical_code)
                                                <span class="badge badge-info">Físico</span>
                                            @endif
                                        </td>
                                        <td>{{ $freepass->status }}</td>
                                        <td>
                                            @if ($freepass->code && $freepass->email)
                                                Enviado a:
                                                <span class="text-info">
                                                    <strong>{{ $freepass->email }}</strong>
                                                </span>
                                                {{ $freepass->updated_at->diffForHumans() }}
                                                <button class="btn btn-info btn-send" data-toggle="modal" data-target="#sendFreepass-{{ $loop->iteration }}"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                                <!-- Modal -->
                                                <form action="{{ route('freepasses.send', [$user, 'Admin']) }}" method="POST">
                                                    @csrf
                                                    <div class="modal fade" id="sendFreepass-{{ $loop->iteration }}" tabindex="-1" role="dialog" aria-labelledby="sendFreepassLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="sendFreepassLabel">Enviar freepass</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true"><i class="fa fa-times"></i></span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Codigo: <strong>{{ $freepass->physical_code ? $freepass->physical_code : $freepass->code }}</strong>
                                                                </p>

                                                                <input type="hidden" name="freepass_id" value="{{ $freepass->id }}">

                                                                <div class="form-group">
                                                                    <label for="email">Dirección de correo electrónico</label>
                                                                    <input type="email" name="email" class="form-control send-mail">
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                <button type="submit" class="btn btn-primary">Enviar</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                @elseif($freepass->code)
                                                <button class="btn btn-primary btn-send" data-toggle="modal" data-target="#sendFreepass-{{ $loop->iteration }}"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar a alguien</button>
                                                <!-- Modal -->
                                                <form action="{{ route('freepasses.send', [$user, 'Admin']) }}" method="POST">
                                                    @csrf
                                                    <div class="modal fade" id="sendFreepass-{{ $loop->iteration }}" tabindex="-1" role="dialog" aria-labelledby="sendFreepassLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="sendFreepassLabel">Enviar freepass</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true"><i class="fa fa-times"></i></span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Codigo: <strong>{{ $freepass->physical_code ? $freepass->physical_code : $freepass->code }}</strong>
                                                                </p>

                                                                <input type="hidden" name="freepass_id" value="{{ $freepass->id }}">

                                                                <div class="form-group">
                                                                    <label for="email">Dirección de correo electrónico</label>
                                                                    <input type="email" name="email" class="form-control send-mail">
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                <button type="submit" class="btn btn-primary">Enviar</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @else
                                                Entregado a: {{ $freepass->name }}
                                            @endif
                                        </td>
                                        <td>{{ $freepass->expiration_date }}</td>
                                        <td>
                                            <form action="{{ route('freepasses.cancel', [$user, 'Admin', $freepass]) }}" method="POST">
                                                @csrf
                                                {{-- <a href="#" class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i> Ver</a> --}}
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cancelar</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <h3>No tiene freepasses activos</h3>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                        {{-- Lista de freepasses (Canjeados)  --}}
                        <div class="tab-pane fade" id="redeemed" role="tabpanel" aria-labelledby="redeemed-tab">

                            <h2 class="text-center my-3">
                                Lista de freepasses <span class="text-success"><strong>Canjeados</strong></span>
                            </h2>
                            <table class="table dataTable text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Canjeado por</th>
                                        <th scope="col">Fecha de vencimiento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($redeemed as $freepass)
                                    <tr>
                                        <td>{{ $freepass->code ? $freepass->code : $freepass->physical_code }}</td>
                                        <td>{{ $freepass->status }}</td>
                                        <td>{{ $freepass->name ? $freepass->name : 'No se especificó' }}</td>
                                        <td>{{ $freepass->expiration_date }}</td>
                                        <td>

                                        </td>
                                    </tr>
                                    @empty
                                        <h3>No tiene freepasses canjeados</h3>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                        {{-- Lista de freepasses (Vencidos)  --}}
                        <div class="tab-pane fade" id="expired" role="tabpanel" aria-labelledby="expired-tab">

                            <h2 class="text-center my-3">
                                Lista de freepasses <span class="text-danger"><strong>Vencidos</strong></span>
                            </h2>
                            <table class="table dataTable text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Fecha de vencimiento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($expired as $freepass)
                                    <tr>
                                        <td>
                                            {{ $freepass->code }}
                                            @if ($freepass->physical_code)
                                                <span class="badge badge-info">Físico</span>
                                            @endif
                                        </td>
                                        <td>{{ "VENCIDO" }}</td>
                                        <td>{{ $freepass->expiration_date }}</td>
                                        <td>

                                        </td>
                                    </tr>
                                    @empty
                                        <h3>No tiene freepasses vencidos</h3>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                        {{-- Lista de freepasses (Cancelados)  --}}
                        <div class="tab-pane fade" id="cancelled" role="tabpanel" aria-labelledby="cancelled-tab">

                            <h2 class="text-center my-3">
                                Lista de freepasses <span class="text-secundary"><strong>Cancelados</strong></span>
                            </h2>
                            <table class="table dataTable text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Fecha de vencimiento</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($cancelled as $freepass)
                                    <tr>
                                        <td>
                                            {{ $freepass->code ? $freepass->code : $freepass->physical_code }}
                                            @if ($freepass->physical_code)
                                                <span class="badge badge-info">Físico</span>
                                            @endif
                                        </td>
                                        <td>{{ $freepass->status }}</td>
                                        <td>{{ $freepass->expiration_date }}</td>
                                        <td>

                                        </td>
                                    </tr>
                                    @empty
                                        <h3>No tiene freepasses cancelados</h3>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>


@endsection
