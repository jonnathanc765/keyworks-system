@extends('frontend.layouts.app')

@section('content')

<section class="container">


    <div class="form-container">


        <div class="logo">
            <img src="{{ asset('img/logo.png') }}" alt="">
        </div>



        <h3>¡Gracias por aceptar nuestra invitación!</h3>

        @if (Session::has('success'))

            <a href="https://ikeyworks.com" class="success">Página de inicio</a>

        @else

            <h4>Ahora solo debes confirmar tus datos y listo.</h4>
            <form action="{{ route('contact.store') }}" method="POST">
                {{ csrf_field() }}
                <div>
                    <label for="name">Nombre y apellido</label>
                    <input type="text" name="name" value="{{ old('name') }}">
                    @error('name')
                    <div class="error">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div>
                    <label for="phone">Teléfono</label>
                    <input type="text" name="phone" value="{{ old('phone') }}">
                    @error('phone')
                    <div class="error">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="submit-container">
                    <button class="submit">Enviar</button>
                </div>
            </form>

        @endif


        <p>¡Te esperamos el sábado 18!</p>

    </div>

</section>

@endsection
