<table border="0" cellpadding="0" cellspacing="0" height="100%" lang="es" style="min-width:348px;" width="100%">
    <tbody>
        <tr height="32" style="height:32px">
            <td>
            </td>
        </tr>
        <tr align="center">
            <td>
                <div>
                    <div>
                    </div>
                </div>
                <table border="0" cellpadding="0" cellspacing="0"
                    style="padding-bottom:20px;max-width:600px;min-width:300px">
                    <tbody>
                        <tr>
                            <td style="width:8px" width="8">
                            </td>
                            <td>
                                <div align="center"
                                    style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px; background-color: #fff;
                                    font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif;
        color:rgba(0,0,0,0.87);
        font-size:14px;
        line-height:20px;">
                                    <img src="{{ asset('img/logo.png') }}" style="width:20% ;margin-bottom:16px;" alt="Logo de keyworks en negro sin fondo">
                                    <div
                                        style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word">
                                        <div style="font-size:24px">
                                            Te han enviado un freepass desde {{ config('app.name') }}
                                        </div>
                                        <table align="center" style="margin-top:8px">
                                            <tbody>
                                                <tr style="line-height:normal">
                                                    <td>
                                                        <a
                                                            style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.87);font-size:14px;line-height:20px">
                                                            Enviado por: <b>{{ $user->name }}</b>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="freepass" style="width: 100%;
                                    padding: 40px 0;">

                                        <div class="card" style="width: 100%;
                                        background-color: #fff200;
                                        border: 1px solid #000;
                                        max-width: 300px;
                                        padding: 20px;">

                                            <div class="logo" style="width: 100%;">
                                                <img src="{{ asset('img/logo.png') }}" class="logo" style="width: 150px;" alt="Logo de keyworks en negro sin fondo"/>
                                            </div>
                                            <p style="font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif; margin-top: 25px; color: #000">
                                                Con este <strong>freepass</strong> puedes acceder un dia a nuestras instalaciones
                                            </p>



                                            <div class="down-arrow" style="margin-top: 15px;">
                                                <img src="{{ asset('img/icons/flecha-abajo.png') }}" alt="flecha abajo" style="width: 100%;
                                                max-width: 180px; width: 35px">

                                            </div>


                                            <div class="svg-container" style="width: 100%;margin-top: 15px;margin-bottom: 15px;">

                                                <img src="{{ asset('img/icons/qr.png') }}" alt="codigo qr" style=" width: 150px;">


                                            </div>
                                            <p style="font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif; margin-bottom: 10px;">
                                                <strong>{{ $freepass->code }}</strong>
                                            </p>

                                            <p style="font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif; margin-bottom: 10px; color: #000;">Para mas información <br><strong>escríbenos</strong></p>

                                    <div style="flex-direction: column; align-items:center; display :flex;">
                                                <div style="display: flex; justify-content: center; align-items: center;">

                                                    <img src="{{ asset('img/icons/flecha-lateral.png') }}" width="18px" alt="flecha lateral derecha">

                                                    <div class="icon" style="margin-right: 10px;
                                                    margin-left: 5px;">

                                                        <img src="{{ asset('img/icons/logo-whatsapp2.png') }}" width="30px" alt="logo de whatsapp">

                                                    </div>

                                                    <a href="tel:+584121728731" style="font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif; color: #000;font-weight: 600;text-decoration: none;">0424 5494828</a>
                                                </div>
                                                <div class="item" style="display: flex;
                                                justify-content: center;
                                                align-items: center;">

                                                    <img src="{{ asset('img/icons/flecha-lateral.png') }}" width="18px" alt="flecha lateral derecha">

                                                    <div class="icon" style="margin-right: 10px;
                                                    margin-left: 5px;">

                                                        <img src="{{ asset('img/icons/logo-correo.png') }}" width="30px" alt="logo de correo">

                                                    </div>
                                                    <a href="mailto:info@ikeyworks.com" style="font-family:'Google Sans', Roboto,RobotoDraft, Helvetica,Arial,sans-serif;color: #000;
                                                    font-weight: 600;
                                                    text-decoration: none;">info@ikeyworks.com</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div style="text-align:left">
                                    <div
                                        style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">
                                        <div>
                                            Has recibido este correo electrónico porque un usuario te ha enviado un freepass desde {{ config('app.name') }}. Si no sabes de que trata este correo,
                                            ponte en contacto con el soporte técnico de {{ config('app.name') }}.
                                        </div>
                                        <div style="direction:ltr">
                                            <a
                                                style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">
                                                © Copyright 2020 {{ config('app.name') }} C.A. Todos los derechos
                                                reservados.
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="width:8px" width="8">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
