Te han enviado un freepass desde {{ config('app.name') }}.

Con este freepass tendras acceso a nuestras instalaciones por un dia.

El codigo del freepass es: {{ $freepass->code }}

Enviado por {{ $user->name }}
