@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Configuración</h2>

                <form action="{{ route('config.update') }}" method="POST">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="ci">Cédula</label>
                        <input type="text" disabled class="form-control" id="ci" value="{{ $user->ci }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $user->name }}">
                    </div>

                    <div class="form-group">
                        <label for="email">Correo</label>
                        <input type="text" disabled class="form-control" id="email" value="{{ $user->email }}">
                    </div>

                    <div class="form-group">
                        <label for="phone">Teléfono</label>
                        <input type="text" disabled class="form-control" id="phone" value="{{ $user->phone }}">
                    </div>

                    <div class="form-group">
                        <label for="organization">Organización</label>
                        <input type="text" class="form-control @error('organization') is-invalid @enderror" id="organization" name="organization" value="{{ $user->organization }}">
                        @error('organization')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="address">Dirección</label>
                        <input type="text" disabled class="form-control" id="address" value="{{ $user->address }}">
                    </div>

                    <div class="form-group">
                        <label for="job">Ocupación</label>
                        <input type="text" class="form-control @error('job') is-invalid @enderror" id="job" name="job" value="{{ $user->job }}">
                        @error('job')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="deffer_code">Código referido</label>
                        <input type="password" disabled class="form-control" id="deffer_code" value="{{ $user->deffer_code }}">
                    </div>

                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Repita la contraseña </label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                    </div>

                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>

@endsection
