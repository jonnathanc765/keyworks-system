{{-- SweetAlert2s --}}
@if (Session::has('success'))
<script>
        Swal.fire("¡Todo salió bien!", "{{ Session::get('success') }}", "success");
</script>
@endif

@if (Session::has('errors'))
<script>
    Swal.fire("Tenemos un problema...", "{{Session::get('error')}}", "error");
</script>
@endif

@if (Session::has('info'))
<script>
    Swal.fire("{{ Session::get('info') }} :)", "¡Todo salió bien!", "success");
</script>
@endif

@if (session('status'))
<script>
    Swal.fire("{{ session('status') }}");
</script>
@endif

@if (Session::has('error'))
<script>
    Swal.fire("Tenemos un problema...", "{{Session::get('error')}}", "error");
</script>
@endif
