<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fbed21" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>

    @yield('content')

</body>
</html>
