@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="my-2">Lista de planes</h2>
                    <a href="{{ route('admin.plans.create') }}" class="btn btn-success btn-lg float-right">Crear nuevo plan</a>
                    <table class="table dataTable text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Dias</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($plans as $plan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $plan->name }}</td>
                                <td>$ {{ $plan->price }}</td>
                                <td>{{ $plan->is_monthly ? 'Mensual' : $plan->days }}</td>
                                <td>
                                    <form action="{{ route('admin.plans.destroy', $plan ) }}" method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <a href="{{ route('admin.plans.show', $plan) }}" class="btn btn-primary">Ver</a>
                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                                <h3>No hay planes registrados</h3>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
