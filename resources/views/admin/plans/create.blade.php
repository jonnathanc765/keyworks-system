@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="text-center">
                        Crear nuevo plan
                    </h2>

                    <form action="{{ route('admin.plans.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre *</label>
                            <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">Descripcion *</label>
                            <textarea cols="30" rows="10" name="description" id="description" class="form-control @error('description')is-invalid @enderror">{{ old('description') }}</textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="ambassador_charge">Cargo para el embajador *</label>
                            <input type="text" name="ambassador_charge" id="ambassador_charge" class="form-control @error('ambassador_charge')is-invalid @enderror" value="{{ old('ambassador_charge', 0) }}">
                            <small class="form-text text-muted">
                                Esta es la cantidad que gana el embajador por cada diferido.
                            </small>
                            @error('ambassador_charge')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="keyworker_charge">Cargo para el Keyworker *</label>
                            <input type="text" name="keyworker_charge" id="keyworker_charge" class="form-control @error('keyworker_charge') is-invalid @enderror" value="{{ old('keyworker_charge', 0) }}">
                            <small class="form-text text-muted">
                                Esta es la cantidad que se descuenta al keyworker por un codigo de diferido valido si elige este plan.
                            </small>
                            @error('keyworker_charge')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="custom-control custom-checkbox my-2">
                            <input {{ old('is_monthly') ? 'checked' : '' }} type="checkbox" name="is_monthly" class="custom-control-input @error('is_monthly') is-invalid @enderror" id="is_monthly">
                            <label class="custom-control-label" for="is_monthly">¿Este plan es mensual?</label>
                            @error('is_monthly')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group days">
                            <label for="days">Dias</label>
                            <input type="text" name="days" id="days" class="form-control @error('days')is-invalid @enderror" value="{{ old('days') }}">
                            @error('days')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="included_freepasses">Freepasses incluidos</label>
                            <input type="text" name="included_freepasses" id="included_freepasses" class="form-control @error('included_freepasses')is-invalid @enderror" value="{{ old('included_freepasses', 0) }}">
                            @error('included_freepasses')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="price">Precio * </label>
                            <input type="text" name="price" id="price" class="form-control @error('price')is-invalid @enderror" value="{{ old('price') }}">
                            @error('price')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function () {

            change();

            $('#is_monthly').change(function () {
                $('.days input').val("");

                change();

            });

            function change () {
                if ($('#is_monthly').is(':checked')) {
                    $('.days').fadeOut();
                } else {
                    $('.days').fadeIn();
                }
            }

        });

    </script>
@endsection
