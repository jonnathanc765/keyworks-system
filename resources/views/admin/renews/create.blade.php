@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="my-2">Renovar plan de {{ $user->name }}</h2>

                    Último plan: <strong>{{ $lastPlan->plan_name }} (Vencido el {{ $lastPlan->expiration_date }})</strong>

                    <form action="{{ route('admin.renews.store', $user) }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="plan">Plan nuevo</label>
                            <select name="plan_id" id="plan" class="form-control @error('plan')is-invalid @enderror">
                                @forelse ($plans as $plan)
                                    <option value="{{ $plan->id }}"
                                        {{ old('plan_id') == $plan->id ? 'selected' : '' }}
                                        >{{  $plan->name }}</option>
                                @empty
                                <option value="">No hay planes registrados</option>
                                @endforelse
                            </select>
                            @error('plan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="selected-plan invoice-template">
                            <h3 class="text-center">Detalles</h3>
                            <ul>
                                <li>
                                    Nombre del plan seleccionado: <span class="plan-name"></span>
                                </li>
                                <li>
                                    Descripción del plan seleccionado:
                                </li>
                                <li>
                                    <ul>
                                        <li>
                                            <strong>
                                                <span class="plan-description"></span>
                                            </strong>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    Precio del plan seleccionado: $ <span class="plan-price"></span>
                                </li>
                                <li>
                                    Mensual: <span class="plan-is_monthly"></span>
                                </li>
                                <li>
                                    Dias: <span class="plan-days"></span>
                                </li>
                            </ul>
                        </div>

                        <div class="form-group">
                            <label for="start_date">Fecha de inicio</label>
                            <input value="{{ old('start_date') }}" type="date" class="form-control @error('start_date') is-invalid @enderror" name="start_date" id="start-date">
                            <small class="text-muted">Nota: Si no se especifica fecha de inicio, se tomará la fecha actual, esto es tomado en cuenta, solo cuando el plan es mensual (No consumible)</small>
                            @error('start_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div>
                            <button type="submit" class="btn btn-primary">Renovar</button>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

    <script>

        $(document).ready(function () {

            var plans = @json($plans) ;

            updatePlanDetails();

            $('#plan').change(function () {

                updatePlanDetails();

            });

            function updatePlanDetails() {

                current_plan_id = $('#plan').val();

                current_plan = plans.filter(function (plan) {
                    return plan.id == current_plan_id;
                });

                current_plan = current_plan[0];

                $('.plan-name').text(current_plan.name);
                $('.plan-description').text(current_plan.description);
                $('.plan-price').text(current_plan.price);
                $('.plan-name').text(current_plan.name);
                $('.plan-days').text(current_plan.days);

                var is_monthly = ""
                if (current_plan.is_monthly == 0) {
                    is_monthly = "No";
                } else {
                    is_monthly = "Si";
                }

                $('.plan-is_monthly').text(is_monthly);


            }


        })

    </script>

@endsection
