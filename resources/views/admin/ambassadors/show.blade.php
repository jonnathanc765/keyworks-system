@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="my-2 text-center">Detalles de embajador</h2>

                    <h4 class="text-right my-2 text-danger">Codigo de embajador: {{ $ambassador->ambassador_code }}</h4>

                    <form action="{{ route('admin.ambassadors.update', $ambassador) }}" method="POST">

                        {{ method_field('PUT') }}
                        @csrf

                        <h3>
                            Datos personales
                        </h3>

                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name', $ambassador->user->name) }}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="ci">Cédula</label>
                            <input type="text" name="ci" id="ci" class="form-control @error('ci')is-invalid @enderror" value="{{ old('ci', $ambassador->user->ci) }}">
                            @error('ci')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Correo *</label>
                            <input type="text" name="email" id="email" class="form-control @error('email')is-invalid @enderror" value="{{ old('email', $ambassador->user->email) }}">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="phone">Teléfono *</label>
                            <input type="text" name="phone" id="phone" class="form-control @error('phone')is-invalid @enderror" value="{{ old('phone', $ambassador->user->phone) }}">
                            @error('phone')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="address">Dirección de habitación</label>
                            <input type="text" name="address" id="address" class="form-control @error('address')is-invalid @enderror" value="{{ old('address', $ambassador->user->address) }}">
                            @error('address')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="deffer_code">Código de referido</label>
                            <input type="text" name="deffer_code" id="deffer_code" class="form-control @error('deffer_code')is-invalid @enderror" value="{{ old('deffer_code', $ambassador->user->deffer_code) }}">
                            @error('deffer_code')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <h3>Datos de embajador</h3>

                        <div class="form-group">
                            <label for="organization">Organización</label>
                            <input type="text" name="organization" id="organization" class="form-control @error('organization')is-invalid @enderror" value="{{ old('organization', $ambassador->user->organization) }}">
                            @error('organization')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="instagram_account">Instagram</label>
                            <input type="text" name="instagram_account" id="instagram_account" class="form-control @error('instagram_account')is-invalid @enderror" value="{{ old('instagram_account', $ambassador->instagram_account) }}">
                            @error('instagram_account')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="another_social_account">Otra red social</label>
                            <input type="text" name="another_social_account" id="another_social_account" class="form-control @error('another_social_account')is-invalid @enderror" value="{{ old('another_social_account', $ambassador->another_social_account) }}">
                            @error('another_social_account')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>



                        {{-- <div class="form-group">
                            <label for="freepasses_quantity">Cantidad de Freepasses</label>
                            <input type="text" name="freepasses_quantity" id="freepasses_quantity" class="form-control @error('freepasses_quantity')is-invalid @enderror" value="{{ old('freepasses_quantity', 10) }}">
                            @error('freepasses_quantity')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div> --}}


                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
