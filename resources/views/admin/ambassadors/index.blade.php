@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('admin.ambassadors.create') }}" class="btn btn-success btn-lg float-right">Nuevo embajador</a>
                    <h2 class="my-2">Lista de embajadores</h2>
                    <table class="table dataTable text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Organización</th>
                                <th scope="col">Monto Acumulado</th>
                                <th scope="col">Freepases Activados</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ambassadors as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->organization }}</td>
                                <td>$ {{ $user->ambassador->accumulated_amount }}</td>
                                <td>{{ $user->activeFreepassesType('Ambassador')->count() }}</td>
                                <td>
                                    <form action="{{ route('admin.ambassadors.destroy', $user->ambassador) }}" method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <a href="{{ route('admin.ambassadors.show', $user->ambassador->id) }}" class="btn btn-primary">Ver</a>
                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
