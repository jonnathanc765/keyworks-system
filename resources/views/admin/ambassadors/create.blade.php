@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2 class="text-center">Registrar nuevo embajador</h2>

                        {{-- @if ($errors->any())
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif --}}

                    <form action="{{ route('admin.ambassadors.store') }}" method="POST">

                        @csrf

                        <div class="custom-control custom-checkbox my-3">
                            <input {{ old('is_keyworker') ? 'checked' : '' }} type="checkbox" class="custom-control-input" name="is_keyworker" id="is_keyworker">
                            <label class="custom-control-label" for="is_keyworker">¿Es un keyworker ya registrado?</label>
                        </div>

                        <div class="form-group user-list">
                            <label for="user_id">Usuario registrado</label>
                            <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                @forelse ($users as $user)
                                <option value="{{ $user->id }}"
                                    {{ old('user_id') ? 'checked' : '' }}
                                    >{{ $user->ci }} {{ $user->name }}</option>
                                @empty
                                <option value="" selected disabled>No hay keyworkers registrados</option>
                                @endforelse
                            </select>
                            @error('user_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="user-data">

                            <div class="form-group">
                                <label for="name">Nombre *</label>
                                <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name') }}">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input type="text" name="phone" id="phone" class="form-control @error('phone')is-invalid @enderror" value="{{ old('phone') }}">
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="organization">Organización</label>
                                <input type="text" name="organization" id="organization" class="form-control @error('organization')is-invalid @enderror" value="{{ old('organization') }}">
                                @error('organization')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="instagram_account">Instagram</label>
                            <input type="text" name="instagram_account" id="instagram_account" class="form-control @error('instagram_account')is-invalid @enderror" value="{{ old('instagram_account') }}">
                            @error('instagram_account')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="another_social_account">Otra red social</label>
                            <input type="text" name="another_social_account" id="another_social_account" class="form-control @error('another_social_account')is-invalid @enderror" value="{{ old('another_social_account') }}">
                            @error('another_social_account')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>


                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function () {

            change();

            $('#is_keyworker').change(function () {
                change();
            });


            function change () {
                if ($('#is_keyworker').is(':checked')) {
                    $('.user-list').fadeIn();
                    $('.user-data').hide();
                } else {
                    $('.user-list').hide();
                    $('.user-data').fadeIn();
                }
            }



        });

    </script>
@endsection
