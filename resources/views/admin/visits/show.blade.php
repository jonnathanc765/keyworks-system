@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="my-2">Lista de visitas de este plan</h2>
                    <table class="table dataTable text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($visits as $visit)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $visit->created_at->format('d-m-Y') }}</td>
                                <td>{{ $visit->description }}</td>
                                <td>
                                    <form action="{{ route('admin.visits.destroy', $visit) }}" method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                                <h3>No hay planes registrados</h3>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
