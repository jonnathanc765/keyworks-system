@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <h2 class="text-center">
                        Registrar una nueva visita
                    </h2>
                    {{-- @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif --}}
                    <form action="{{ route('admin.visits.store', $user) }}" method="POST">
                        @csrf

                        @if (isset($user))
                        <p>
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            Usuario: {{ $user->name }}
                        </p>
                        @else
                        <div class="form-group">
                            <label for="user_id">Usuario *</label>
                            <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                <option value="" selected>Elija una usuario</option>
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}"
                                    @if (old('user_id') == $user->id)
                                    selected
                                    @endif
                                    >{{ $user->name }}</option>
                                @endforeach
                                @error('user_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </select>
                            <small>Solo se pueden visualizar los usuarios a los cuales se le pueden contabilizar visitas, de tener planes mensuales activos que aun no expiran, no se podrán contar visitas</small>
                        </div>
                        @endif

                        <div class="form-group">
                            <label for="days">Dias consumidos *</label>
                            <input type="text" value="{{ old('days', 1) }}" name="days" id="days" class="form-control @error('days') is-invalid @enderror">
                            @error('days')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="date">Fecha de visita</label>
                            <input type="date" value="{{ old('date') }}" name="date" id="date" class="form-control @error('date') is-invalid @enderror">
                            @error('date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <small>Si no se es especificada una fecha, se tomara la fecha actual</small>
                        </div>


                        <div class="form-group">
                            <label for="description">Descripcion de la visita</label>
                            <input type="text" value="{{ old('description') }}" name="description" id="description" class="form-control @error('description') is-invalid @enderror">
                            @error('description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Rgistrar visita</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
