@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2>Control de freepasses</h2>

            <h4>Generar nuevos freepasses</h4>

            Usuario: {{ $user->name }}
            Freepasses actualmente activos: {{ $user->activeFreepassesType($user_type)->count() }}

            <form action="{{ route('admin.freepasses.store', [$user, $user_type]) }}" method="POST">
                @csrf

                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" {{ old('is_physical') ? 'checked' : '' }} name="is_physical" class="custom-control-input" id="is_physical">
                    <label class="custom-control-label" for="is_physical">¿Los freepasses son físicos?</label>
                </div>

                <div class="form-group quantity">
                    <label for="quantity">Cantidad de freepasses a generar</label>
                    <input type="number" class="form-control @error('quantity') is-invalid @enderror" name="quantity" id="quantity">
                    @error('quantity')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>



                <div class="is-physical">

                    <div class="form-group">
                        <label for="freepasses_number">Ingrese el numero de freepass (Si son varios, separelos por comas. Ejemplo: 5,3,8,7)</label>
                        <input type="text" value="{{ old('freepasses_number') }}" name="freepasses_number" class="form-control @error('freepasses_number') is-invalid @enderror" id="freepasses_number">
                        @error('freepasses_number')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="name">Nombre *</label>
                        <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name') }}">
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="phone">Teléfono *</label>
                        <input type="text" name="phone" id="phone" class="form-control @error('phone')is-invalid @enderror" value="{{ old('phone') }}">
                        @error('phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Correo *</label>
                        <input type="text" name="email" id="email" class="form-control @error('email')is-invalid @enderror" value="{{ old('email') }}">
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    </div>





                <button type="submit" class="btn btn-primary">Generar</button>
            </form>

        </div>
    </div>
</div>


@endsection

@section('scripts')

<script>

    changeIsPhysical();


    $('#is_physical').change(function () {
        changeIsPhysical();
    });

    function changeIsPhysical () {

        if ($('#is_physical').is(':checked')) {
            $('.quantity').fadeOut(200, function () {
                $('.is-physical').fadeIn();
            });
        } else {
            $('.is-physical').fadeOut(200, function () {
                $('.quantity').fadeIn();
            });
        }

    }


</script>

@endsection
