@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            @if (Session::has('success'))
            <h2 class="text-center my-5">
                {{ Session::get('success') }}
            </h2>
            @else
            <h2 class="text-center my-5">
                Algo anda mal
            </h2>
            @endif

            <p class="text-center my-5">
                <a href="{{ route('freepasses.index', [Auth::user(), 'Admin']) }}" class="btn btn-lg btn-primary">Volver a lista de freepasses</a>
                <a href="{{ route('admin.freepasses.redeem.create') }}" class="btn btn-lg btn-info">Canjear freepass</a>
            </p>

        </div>
    </div>
</div>

@endsection
