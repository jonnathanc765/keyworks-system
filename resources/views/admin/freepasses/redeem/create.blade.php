@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">


            <div class="card">
                <div class="card-body">

                    <h2>Canjear freepass</h2>

                    <form action="{{ route('admin.freepasses.redeem.store') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="freepass_code">Ingrese el codigo del freepass</label>
                            <input type="text" name="freepass_code" class="form-control @error('freepass_code') is-invalid @enderror" id="freepass_code" value="{{ old('freepass_code') }}">
                            @error('freepass_code')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}">
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="phone">Teléfono</label>
                            <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}">
                            @error('phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>


                        <button type="submit" class="btn btn-primary">Hacer canje</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
