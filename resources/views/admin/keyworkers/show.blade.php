@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    <h2 class="my-2 text-center">Keyworker {{ $user->name }}</h2>
                    <h4 class="text-right">Dias restantes: {{ $user->remainingDays() }}</h4>


                    <div id="accordion">

                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h4 class="mb-0">
                                    <button class="btn btn-default" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Detalles del Keyworker (Expandir/Ocultar)
                                    </button>
                                </h4>
                            </div>

                            <div id="collapseOne" class="collapse {{ Session::has('errors') ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <form action="{{ route('admin.keyworkers.update', $user) }}" method="POST">

                                        @csrf
                                        {{ method_field('PUT') }}
                                        <div class="form-group">
                                            <label for="name">Nombre *</label>
                                            <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name', $user->name) }}">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="ci">Cédula</label>
                                            <input type="text" name="ci" id="ci" class="form-control @error('ci')is-invalid @enderror" value="{{ old('ci', $user->ci) }}">
                                            @error('ci')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Teléfono</label>
                                            <input type="text" name="phone" id="phone" class="form-control @error('phone')is-invalid @enderror" value="{{ old('phone', $user->phone) }}">
                                            @error('phone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Correo electrónico</label>
                                            <input type="email" name="email" id="email" class="form-control @error('email')is-invalid @enderror" value="{{ old('email', $user->email) }}">
                                            @error('email')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="job">Ocupación</label>
                                            <input type="text" name="job" id="job" class="form-control @error('job')is-invalid @enderror" value="{{ old('job', $user->job) }}">
                                            @error('job')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="address">Dirección de habitación *</label>
                                            <input type="text" name="address" id="address" class="form-control @error('address')is-invalid @enderror" value="{{ old('address', $user->address) }}">
                                            @error('address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="organization">Organización</label>
                                            <select name="organization" id="organization" class="form-control">
                                                <option value="" selected>Sin organizacíon</option>
                                                @foreach ($organizations as $organization)
                                                <option value="{{ $organization->name }}"
                                                    {{ old('organization', $user->organization) == $organization->name ? 'selected' : '' }}
                                                    >{{ $organization->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('organization')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="deffer_code">Código de referido</label>
                                            <input disabled type="text" id="deffer_code" class="form-control" value="{{ $user->deffer_code }}">
                                        </div>


                                        <button type="submit" class="btn btn-primary">
                                            Guardar
                                        </button>

                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h4 class="mb-0">
                                    <button class="btn btn-default collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Detalles de planes (Expandir/Ocultar)
                                    </button>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="collapse {{ !Session::has('errors') ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">


                        <div class="selected-plan invoice-template">
                            <h4>Plan Mensual</h4>
                            @if ($monthlyPlan)

                                <h4>(Inició el {{ Carbon\Carbon::parse($monthlyPlan->expiration_date)->sub(1, 'months')->format('d-m-Y') }})</h4>

                                <ul>
                                    <li>
                                        Nombre del plan seleccionado: <span class="plan-name">{{ $monthlyPlan->plan_name }}</span>
                                    </li>
                                    <li>
                                        Descripción del plan seleccionado:
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <strong>
                                                    {{ $monthlyPlan->notes }}
                                                </strong>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Precio del plan pagado:
                                        <strong>
                                            $ {{ $monthlyPlan->amount }}
                                        </strong>
                                    </li>
                                    <li>
                                        Fecha de vencimiento:
                                        <strong>
                                            {{ Carbon\Carbon::parse($monthlyPlan->expiration_date)->format('d-m-Y') }}
                                        </strong>
                                    </li>
                                </ul>
                                @if ($monthlyPlan->is_finished())
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="alert alert-danger text-center">
                                            Plan vencido
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @else
                                <h5 class="text-center">No disponible</h5>
                            @endif
                            <h4>Plan Consumible</h4>

                            @if ($consumablePlan)
                            <h4>(Se registró el {{ $consumablePlan->created_at->format('d-m-Y') }})</h4>
                                <ul>
                                    <li>
                                        Nombre del plan seleccionado: <span class="plan-name">{{ $consumablePlan->plan_name }}</span>
                                    </li>
                                    <li>
                                        Descripción del plan seleccionado:
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <strong>
                                                    {{ $consumablePlan->notes }}
                                                </strong>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Precio del plan pagado:
                                        <strong>
                                            $ {{ $consumablePlan->amount }}
                                        </strong>
                                    </li>
                                    <li>
                                        Dias:
                                        <strong>
                                            {{ $consumablePlan->days }}
                                        </strong>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.visits.show', $consumablePlan) }}" class="btn btn-primary">Ver visitas</a>
                                    </li>
                                </ul>

                                @if ($consumablePlan->is_finished())
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="alert alert-danger text-center">
                                            Plan consumido
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @else
                                <h5 class="text-center">No disponible</h5>
                            @endif


                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h4 class="mb-0">
                                    <button class="btn btn-default collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Renovar plan (Expandir/Ocultar)
                                    </button>
                                </h4>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body text-center">
                                    <h4 class="text-center my-5">Comming Soon</h4>

                                    <a href="{{ route('admin.renews.create', $user) }}" class="btn btn-primary btn-lg my-5">Renovar plan a este keyworker</a>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

