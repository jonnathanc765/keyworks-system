@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2 class="text-center">Registrar nuevo keyworker</h2>

                    {{-- @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif --}}

                    <form action="{{ route('admin.keyworkers.store') }}" method="POST">

                        @csrf

                        <div class="custom-control custom-checkbox my-3">
                            <input {{ old('is_ambassador') ? 'checked' : '' }} type="checkbox" class="custom-control-input" name="is_ambassador" id="is_ambassador">
                            <label class="custom-control-label" for="is_ambassador">¿Es un embajador ya registrado?</label>
                        </div>

                        <div class="form-group user-list">
                            <label for="user_id">Usuario registrado</label>
                            <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                @forelse ($ambassadors as $user)
                                <option value="{{ $user->id }}"
                                    {{ old('user_id') ? 'checked' : '' }}
                                    >{{ $user->ci }} {{ $user->name }}</option>
                                @empty
                                <option value="" selected disabled>No hay embajadores registrados</option>

                                @endforelse
                            </select>
                            @error('user_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="user-data">
                            <div class="form-group">
                                <label for="name">Nombre *</label>
                                <input type="text" name="name" id="name" class="form-control @error('name')is-invalid @enderror" value="{{ old('name') }}">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group mb-2">
                                <label for="phone">Teléfono</label>
                                <input type="text" name="phone" id="phone" class="form-control @error('phone')is-invalid @enderror" value="{{ old('phone') }}">
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <h6>Organización</h6>
                            <div class="input-group mb-3">
                                {{-- <label for="organization">Organización</label> --}}
                                <select name="organization" id="organization" class="form-control @error('organization')is-invalid @enderror">
                                    <option value="" selected>Sin organizacíon</option>
                                    @foreach ($organizations as $organization)
                                    <option value="{{ $organization->name }}"
                                        {{ old('organization') == $organization->name ? 'selected' : '' }}
                                        >{{ $organization->name }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#createOrganization">Nueva organización</button>
                                </div>
                                @error('organization')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="ci">Cédula</label>
                            <input type="text" name="ci" id="ci" class="form-control @error('ci')is-invalid @enderror" value="{{ old('ci') }}">
                            @error('ci')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Correo electrónico</label>
                            <input type="text" name="email" id="email" class="form-control @error('email')is-invalid @enderror" value="{{ old('email') }}">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="address">Dirección de habitación *</label>
                            <input type="text" name="address" id="address" class="form-control @error('address')is-invalid @enderror" value="{{ old('address') }}">
                            @error('address')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="job">Ocupación</label>
                            <input type="text" name="job" id="job" class="form-control @error('job')is-invalid @enderror" value="{{ old('job') }}">
                            @error('job')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="plan">Plan a suscribirse</label>
                            <select name="plan_id" id="plan" class="form-control @error('plan')is-invalid @enderror">
                                @foreach ($plans as $plan)
                                    <option value="{{ $plan->id }}"
                                        {{ old('plan_id') == $plan->id ? 'selected' : '' }}
                                        >{{  $plan->name }}</option>
                                @endforeach
                            </select>
                            @error('plan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="selected-plan invoice-template">
                            <h3 class="text-center">Detalles</h3>
                            <ul>
                                <li>
                                    Nombre del plan seleccionado: <span class="plan-name"></span>
                                </li>
                                <li>
                                    Descripción del plan seleccionado:
                                </li>
                                <li>
                                    <ul>
                                        <li>
                                            <strong>
                                                <span class="plan-description"></span>
                                            </strong>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    Precio del plan seleccionado: $ <span class="plan-price"></span>
                                </li>
                                <li>
                                    Mensual: <span class="plan-is_monthly"></span>
                                </li>
                                <li>
                                    Dias: <span class="plan-days"></span>
                                </li>
                            </ul>
                        </div>

                        <div class="form-group">
                            <label for="start_date">Fecha de inicio</label>
                            <input value="{{ old('start_date') }}" type="date" class="form-control @error('start_date') is-invalid @enderror" name="start_date" id="start-date">
                            <small class="text-muted">Nota: Si no se especifica fecha de inicio, se tomará la fecha actual, esto es tomado en cuenta, solo cuando el plan es mensual (No consumible)</small>
                            @error('start_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="text" name="deffer_code" id="deffer_code" value="{{ old('deffer_code') }}" class="form-control @error('deffer_code') is-invalid @enderror" placeholder="Código de referido" aria-label="Código de referido" aria-describedby="deffer_code">
                            <div class="input-group-append">
                                <button class="btn btn-primary verify-button" type="button">Verificar</button>
                            </div>
                        </div>

                        @error('deffer_code')
                        <div class="row my-2 justify-content-center invalid-deffer-code">
                            <div class="col-md-6 col-12">
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            </div>
                        </div>
                        @enderror

                        <div class="row justify-content-center deffer-discount" style="display: none;">
                            <div class="col-md-6 col-12">
                                <div class="alert alert-success">
                                    Descuento por diferido: <strong> $ <span class="price">25</span></strong>
                                </div>
                            </div>
                        </div>

                        <h3 class="text-right">Total a pagar: $ <span class="total-price"></span></h3>



                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="createOrganization" tabindex="-1" role="dialog" aria-labelledby="organizationModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="organizationModal">Nueva organización</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Añade una nueva organización</h3>
                <div class="form-group">
                    <label for="organization_name">Nombre</label>
                    <input type="text" class="form-control" id="organization_name">
                    <p class="text-danger organization-error" style="display:none">Error, ha ocurrido un error</p>
                </div>
                <div class="spinner text-center" style="display:none">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-store-organization">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {


            var plans = @json($plans) ;

            var deffer_discount = 0;

            var current_plan;

            var ambassadors = @json($ambassadors) ;

            $('.btn-store-organization').click(function () {
                storeOrganization();
            });

            function storeOrganization () {

                $('.organization-error').fadeOut();


                var organization_name = $('#organization_name').val();

                if (organization_name.trim() == "") {
                    $('.organization-error').text("Ingrese un nombre válido");
                    $('.organization-error').fadeIn();
                    return;
                }

                $('.modal .spinner').fadeIn();

                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.organizations.store') }}",
                    data: {
                        name: organization_name,
                        _token: "{{ csrf_token() }}"
                    }
                })
                .then(function (response) {
                    $('#createOrganization').modal('hide')
                    var select = document.querySelector('select#organization');
                    $('#organization_name').val("");
                    $('.modal .spinner').fadeOut();

                    select.insertAdjacentHTML('beforeend', `<option value="${response.name}" selected>${response.name}</option>` );
                })
                .catch(function (err) {
                    alert(err.responseJSON.errors.name[0]);
                    $('.modal .spinner').fadeOut();
                });
            }

            changeAmbassador();

            $('#is_ambassador').change(function () {

                changeAmbassador();

            });

            function changeAmbassador () {

                if ($('#is_ambassador').is(':checked')) {
                    $('.user-list').fadeIn();
                    $('.user-data').hide();
                } else {
                    $('.user-list').hide();
                    $('.user-data').fadeIn();
                }
            }


            updatePlanDetails();

            if ($('#deffer_code').val() != "") {
                verifyDeffer();
            }

            $('#deffer_code').keypress(function (event) {
                if (event.keyCode === 10 || event.keyCode === 13) {
                    event.preventDefault();
                    $('.invalid-deffer-code').fadeOut();
                    verifyDeffer();
                }
            });

            $('#plan').change(function () {

                updatePlanDetails();

            });

            var verifyButton = document.querySelector('.verify-button');
            verifyButton.addEventListener('click', function () {
                $('.invalid-deffer-code').fadeOut();
                verifyDeffer();
            });

            function verifyDeffer() {
                var deffer_code = $('#deffer_code').val();

                if (deffer_code.trim() == "") {
                    return alert('Ingrese un código válido');
                }


                $.ajax({
                    url : "{{ route('admin.ambassadors.code') }}",
                    type: "POST",
                    data: {
                        deffer_code : deffer_code,
                        _token: "{{ csrf_token() }}"
                    }
                })
                .done(function(data) {
                    if (data.success) {
                        deffer_discount = current_plan.keyworker_charge;
                        $('.deffer-discount span').text(deffer_discount);
                        $('.deffer-discount').show();
                        updatePlanDetails();
                    } else {
                        alert('No hay coicidencias encontradas');
                    }

                })
                .fail(function(data) {
                    alert('Ha ocurrido un error en la verificación, por favor comuniquese con un administrador');
                });
            }

            function updatePlanDetails() {

                current_plan_id = $('#plan').val();

                current_plan = plans.filter(function (plan) {
                    return plan.id == current_plan_id;
                });

                current_plan = current_plan[0];

                $('.plan-name').text(current_plan.name);
                $('.plan-description').text(current_plan.description);
                $('.plan-price').text(current_plan.price);
                $('.plan-name').text(current_plan.name);
                $('.plan-days').text(current_plan.days);

                var is_monthly = ""
                if (current_plan.is_monthly == 0) {
                    is_monthly = "No";
                } else {
                    is_monthly = "Si";

                }
                $('.plan-is_monthly').text(is_monthly);


                $('.total-price').text(parseFloat(current_plan.price) - deffer_discount);
            }


        });
    </script>

@endsection
