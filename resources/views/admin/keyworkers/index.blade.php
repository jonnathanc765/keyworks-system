@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('admin.keyworkers.create') }}" class="btn btn-success btn-lg float-right">Nuevo keyworker</a>
                    <h2 class="my-2">Lista de keyworkers</h2>
                    <table class="table dataTable text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Organización</th>
                                <th scope="col">Ocupación</th>
                                <th scope="col">Plan</th>
                                <th scope="col">{{ Carbon\Carbon::now()->sub(1, 'month')->format('F') }}</th>
                                <th scope="col">{{ Carbon\Carbon::now()->format('F') }}</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($keyworkers as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->organization }}</td>
                                <td>{{ $user->job }}</td>
                                <td>{{ $user->lastPlan() ? $user->lastPlan()->plan_name : '(Indefinido)'  }}</td>
                                <td>
                                    @if ($user->lastPlan != null)
                                        $ {{ $user->lastPlan->amount }}
                                        @if ($user->lastPlan->is_paid)
                                        <span class="badge badge-success">pagado</span> <br>
                                        <span>{{ $user->lastPlan->paid_date() }}</span>
                                        @else
                                        <a href="{{ route('admin.payments.create', $user->lastPlan) }}">
                                            <span class="badge badge-danger">pendiente</span>
                                        </a>
                                        @endif
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if ($user->currentPlan != null)
                                        $ {{ $user->currentPlan->amount }}
                                        @if ($user->currentPlan->is_paid)
                                        <span class="badge badge-success">pagado</span> <br>
                                        <span>{{ $user->currentPlan->paid_date() }}</span>
                                        @else
                                        <a href="{{ route('admin.payments.create', $user->currentPlan) }}">
                                            <span class="badge badge-danger">pendiente</span>
                                        </a>
                                        @endif
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('admin.keyworkers.destroy', $user) }}" method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        @if ($user->currentPlanAllowsVisits())
                                            <a href="{{ route('admin.visits.create', $user) }}" class="btn btn-info">Registrar Visita</a>
                                        @endif
                                        <a href="{{ route('admin.keyworkers.show', $user) }}" class="btn btn-primary">Ver</a>
                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
