@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2>Notificar pago</h2>

                        <h4>Usuario: <strong>{{ $planUser->user->name }}</strong></h4>
                        <h4>Plan: <strong>{{ $planUser->plan_name }}</strong></h4>
                        <h4>Total: <strong>$ {{ $planUser->amount }}</strong></h4>


                        <form action="{{ route('admin.payments.store', $planUser) }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="paid_ref">Referencia</label>
                                <input type="text" id="paid_ref" value="{{ old('paid_ref') }}" name="paid_ref" class="form-control @error('paid_ref') is-invalid @enderror">
                                @error('paid_ref')
                                <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="pay_method">Método de pago</label>
                                <input type="text" id="pay_method" value="{{ old('pay_method') }}" name="pay_method" class="form-control @error('pay_method') is-invalid @enderror">
                                @error('pay_method')
                                <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="paid_date">Fecha de pago</label>
                                <input type="date" value="{{ old('paid_date') }}" maxlength="{{ Carbon\Carbon::now()->format('m-d-Y') }}"
                                id="paid_date" name="paid_date" class="form-control @error('paid_date') is-invalid @enderror">
                                @error('paid_date')
                                <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                                <small class="form-text text-muted">
                                    Nota: Si no especifica fecha, se tomará la fecha actual.
                                </small>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
