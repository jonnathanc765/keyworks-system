<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_it_shows_the_contact_form()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(route('contact.index'))
                    ->assertSee('Gracias por aceptar nuestra invitación');
        });
    }
}
