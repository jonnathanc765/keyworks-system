<?php

namespace Tests\Feature;

use App\PlanUser;
use App\Traits\TDDUtilities;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentModuleTest extends TestCase
{
    use RefreshDatabase;
    use TDDUtilities;

    public function test_it_shows_payment_form_for_user_plan()
    {

        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control"
        ]);

        $this->get(route('admin.payments.create', $planUser))
        ->assertStatus(200)
        ->assertViewIs('admin.payments.create')
        ->assertViewHas('planUser')
        ->assertSee('Notificar pago')
        ->assertSee('John Doe')
        ->assertSee('Referencia')
        ->assertSee('Método de pago')
        ->assertSee('Fecha de pago')
        ->assertSee('$ 60')

        ;

    }
    public function test_it_register_payment_for_a_plan()
    {

        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control"
        ]);

        $date = Carbon::now()->format('Y-m-d');

        $this
            ->from(route('admin.payments.create', $planUser))
            ->post(route('admin.payments.store', $planUser), [
                'pay_method' => 'Zelle',
                'paid_date' => $date,
                'paid_ref' => 'Ref. 94341'
            ])
            ->assertRedirect(route('admin.keyworkers.show', $planUser->user))
            ->assertSessionHas('success', 'Pago registrado exitosamente');

        $this->assertDatabaseHas('plan_users', [
            'id' => $planUser->id,
            'paid_date' => $date,
            'paid_ref' => 'Ref. 94341',
            'pay_method' => 'Zelle',
            'is_paid' => true
        ]);
    }
    public function test_paid_date_can_be_bull_when_register_a_payment()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control"
        ]);

        $date = Carbon::now()->format('Y-m-d');

        $this
            ->from(route('admin.payments.create', $planUser))
            ->post(route('admin.payments.store', $planUser), [
                'pay_method' => 'Zelle',
                'paid_date' => '',
                'paid_ref' => 'Ref. 94341'
            ])
            ->assertRedirect(route('admin.keyworkers.show', $planUser->user))
            ->assertSessionHas('success', 'Pago registrado exitosamente');

        $this->assertDatabaseHas('plan_users', [
            'id' => $planUser->id,
            'paid_date' => $date,
            'paid_ref' => 'Ref. 94341',
            'pay_method' => 'Zelle',
            'is_paid' => true
        ]);
    }
    public function test_paid_ref_must_be_not_required()
    {
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control"
        ]);

        $date = Carbon::now()->format('Y-m-d');

        $this
            ->from(route('admin.payments.create', $planUser))
            ->post(route('admin.payments.store', $planUser), [
                'pay_method' => 'Zelle',
                'paid_date' => '2020-02-02',
                'paid_ref' => ''
            ])
            ->assertRedirect(route('admin.keyworkers.show', $planUser->user))
            ->assertSessionHas('success', 'Pago registrado exitosamente');

        $this->assertDatabaseHas('plan_users', [
            'id' => $planUser->id,
            'pay_method' => 'Zelle',
            'paid_date' => '2020-02-02',
            'paid_ref' => null,
            'is_paid' => true
        ]);
    }
    public function test_the_payment_must_be_pending_when_load_form()
    {
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control",
            'is_paid' => true
        ]);

        $this->get(route('admin.payments.create', $planUser))
        ->assertRedirect(route('admin.keyworkers.show', $planUser->user))
        ->assertSessionHas('error', 'No puedes acceder a esta página');
    }
    public function test_the_payment_must_be_pending_when_register_A_payment()
    {
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('keyworker');

        $planUser = factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'amount' => 60,
            'plan_name' => "Plan Control",
            'is_paid' => true,
            'pay_method' => 'Bofa',
            'paid_date' => '2020-02-01',
            'paid_ref' => 'Ref. 2525'
        ]);

        $date = Carbon::now()->format('Y-m-d');

        $this
            ->post(route('admin.payments.store', $planUser), [
                'pay_method' => 'Zelle',
                'paid_date' => '2020-02-02',
                'paid_ref' => 'Ref. 5854'
            ])
            ->assertRedirect(route('admin.keyworkers.show', $planUser->user))
            ->assertSessionHas('error', 'No puedes acceder a esta página');

        $this->assertDatabaseHas('plan_users', [
            'id' => $planUser->id,
            'is_paid' => true,
            'pay_method' => 'Bofa',
            'paid_date' => '2020-02-01',
            'paid_ref' => 'Ref. 2525'
        ]);
    }
}
