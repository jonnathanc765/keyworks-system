<?php

namespace Tests\Feature;

use App\PlanUser;
use App\Traits\TDDUtilities;
use App\User;
use App\Visit;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VisitModuleTest extends TestCase
{
   use RefreshDatabase;
   use TDDUtilities;

    public function test_it_allows_visits_to_user()
    {

        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 10,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        $this->assertTrue($user->currentPlanAllowsVisits());

        $this->get(route('admin.keyworkers.index'))
        ->assertStatus(200)
        ->assertViewHas('keyworkers')
        ->assertViewIs('admin.keyworkers.index')
        ->assertSee('Lista de keyworkers')
        ->assertSee('John Doe')
        ->assertSee('Registrar Visita')
        ;

    }
    public function test_it_creates_a_visit()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        $plan = factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 10,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        $this->post(route('admin.visits.store', $user), [
            'user_id' => $user->id,
            'days' => 1,
            'date' => '2020-02-02',
            'description' => 'Visita normal de una sola persona'
        ]);

        $this->assertDatabaseHas('visits', [
            'plan_user_id' => $plan->id,
            'description' => 'Visita normal de una sola persona',
            'created_at' => Carbon::parse('2020-02-02')
        ]);
    }
    public function test_it_shows_a_visits_list()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        $plan = factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 10,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        Visit::create([
            'created_at'=> now(),
            'plan_user_id' => $plan->id,
            'description' => 'Sala 2'
        ]);

        $this->get(route('admin.visits.show', $plan))
        ->assertStatus(200)
        ->assertViewIs('admin.visits.show')
        ->assertViewHas('visits')
        ->assertSee('Lista de visitas de este plan')
        ->assertSee(now()->format('d-m-Y'))
        ->assertSee('Sala 2')

        ;
    }
    public function test_it_deletes_a_visit()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        $plan = factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 10,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        $visit = Visit::create([
            'created_at'=> now(),
            'plan_user_id' => $plan->id,
            'description' => 'Sala 2'
        ]);

        $this
        ->from(route('admin.visits.show', $plan))
        ->delete(route('admin.visits.destroy', $visit))
        ->assertRedirect(route('admin.visits.show', $plan))
        ->assertSessionHas('success', 'Visita borrada exitosamente')
        ;

        $this->assertDatabaseMissing('visits', [
            'plan_user_id' => $plan->id,
            'description' => 'Sala 2'
        ]);
    }
    public function the_plan_must_be_no_replaced_when_a_visit_its_delete()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        $plan = factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 1,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        factory(PlanUser::class)->create([
            'plan_name' => 'Plan F10',
            'days' => 10,
            'user_id' => $user->id,
            'is_monthly' => false,
            'expiration_date' => null
        ]);

        $visit = Visit::create([
            'created_at'=> now(),
            'plan_user_id' => $plan->id,
            'description' => 'Sala 2'
        ]);

        $this
        ->from(route('admin.visits.show', $plan))
        ->delete(route('admin.visits.destroy', $visit))
        ->assertRedirect(route('admin.visits.show', $plan))
        ->assertSessionHas('error', 'No se puede borrar, ya este usuario tiene otro plan vigente')
        ;

        $this->assertDatabaseHas('visits', [
            'plan_user_id' => $plan->id,
            'description' => 'Sala 2'
        ]);
    }
}
