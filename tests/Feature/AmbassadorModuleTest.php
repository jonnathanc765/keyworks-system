<?php

namespace Tests\Feature;

use App\Ambassador;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Traits\TDDUtilities;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AmbassadorModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_shows_the_ambassadors_list()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        factory(User::class)->create([
            'name' => 'Ambassador Test',
            'organization' => 'Google'
        ])->each(function ($user)
        {
            $user->assignRole('ambassador');
            factory(Ambassador::class)->create([
                'accumulated_amount' => 25,
                'user_id' => $user->id
            ]);
        });


        $this->get(route('admin.ambassadors.index'))
        ->assertStatus(200)
        ->assertViewIs('admin.ambassadors.index')
        ->assertViewHas('ambassadors')
        ->assertSee('Lista de embajadores')
        ->assertSee('Ambassador Test')
        ->assertSee('Google')
        ->assertSee('$ 25');
    }

    public function test_it_shows_ambsassador_create_form()
    {

        $this->actingAs($this->createAdminUser());

        factory(User::class)->create([
            'name' => 'Jeremias'
        ])->assignRole('keyworker');

        $this->get(route('admin.ambassadors.create'))
        ->assertStatus(200)
        ->assertViewIs('admin.ambassadors.create')
        ->assertSee('Registrar nuevo embajador')
        ->assertViewHas('users')
        ->assertSee('Jeremias');

        $this->assertTrue(true);

    }

    public function test_it_creates_a_ambassador()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        factory(Ambassador::class)->create([
            'ambassador_code' => 'test-1',
            'user_id' => $user->id
        ]);

        $this->post(route('admin.ambassadors.store'), [
            'name' => 'Gaby Teran',
            'organization' => 'Promar TV',
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
            'phone' => '0416359590'
        ])
        ->assertRedirect(route('admin.ambassadors.index'))
        ->assertSessionHas('success', 'Registrado exitosamente');

        $ambassador = Ambassador::where('instagram_account', 'gaby_teran')->first();

        $this->assertDatabaseHas('ambassadors', [
            'ambassador_code' => 'gaby-'. $ambassador->id,
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
        ]);
        $this->assertDatabaseHas('users', [
            'organization' => 'Promar TV',
            'name' => 'Gaby Teran',
            'phone' => '0416359590'
        ]);

    }
    public function test_it_required_name()
    {
        $this->actingAs($this->createAdminUser());


        $this
        ->from(route('admin.ambassadors.create'))
        ->post(route('admin.ambassadors.store'), [
            'name' => '',
            'organization' => 'Promar TV',
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
            'phone' => '0416359590'
        ])
        ->assertRedirect(route('admin.ambassadors.create'))
        ->assertSessionHasErrors('name');

        $this->assertDatabaseMissing('ambassadors', [
            'ambassador_code' => 'gaby-1',
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
        ]);
        $this->assertDatabaseMissing('users', [
            'organization' => 'Promar TV',
            'name' => 'Gaby Teran',
            'phone' => '0416359590'
        ]);
    }
    public function test_shows_the_ambassador_details()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ]);

        factory(Ambassador::class)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('admin.ambassadors.show', $user->ambassador))
        ->assertStatus(200)
        ->assertViewIs('admin.ambassadors.show')
        ->assertViewHas('ambassador');

    }
    public function test_it_updates_a_ambassador()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John doe',
            'ci' => '25824044',
            'phone' => '04163559590',
            'organization' => 'La maquina de hacer pajaros',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 445',
            'job' => 'Developer'
        ]);

        $ambassador = factory(Ambassador::class)->create([
            'user_id' => $user->id,
            'instagram_account' => 'john_doe',
            'another_social_account' => 'facebook'
        ]);

        $this
        ->from(route('admin.ambassadors.show', $ambassador))
        ->put(route('admin.ambassadors.update', $ambassador), [
            'name' => 'Jonh Travolta',
            'ci' => '25633963',
            'phone' => '04243335598',
            'email' => 'johntravolta@gmail.com',
            'address' => '1600 rivet point 446',
            'job' => 'Developer Jr.',
            'organization' => 'Google',
            'instagram_account' => 'john_travolta',
            'another_social_account' => 'Google_plus',
        ])
        ->assertRedirect(route('admin.ambassadors.show', $ambassador))
        ->assertSessionHas('success', 'Modificado exitosamente');

        $this->assertDatabaseHas('users', [
            'name' => 'Jonh Travolta',
            'ci' => '25633963',
            'organization' => 'Google',
            'phone' => '04243335598',
            'email' => 'johntravolta@gmail.com',
            'address' => '1600 rivet point 446',
            'job' => 'Developer Jr.'
        ]);

        $this->assertDatabaseHas('ambassadors', [
            'instagram_account' => 'john_travolta',
            'another_social_account' => 'Google_plus'
        ]);

    }
    public function test_it_deletes_a_ambassador_model()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John doe',
            'ci' => '25824044',
            'phone' => '04163559590',
            'organization' => 'La maquina de hacer pajaros',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 445',
            'job' => 'Developer'
        ])->assignRole('ambassador');

        $ambassador = factory(Ambassador::class)->create([
            'user_id' => $user->id,
            'instagram_account' => 'john_doe',
            'another_social_account' => 'facebook'
        ]);

        $this
        ->from(route('admin.ambassadors.index'))
        ->delete(route('admin.ambassadors.destroy', $ambassador))
        ->assertRedirect(route('admin.ambassadors.index'))
        ->assertSessionHas('success', 'Borrado exitosamente');

        $this->assertDatabaseHas('users', [
            'name' => 'John doe',
            'ci' => '25824044',
            'phone' => '04163559590',
            'organization' => 'La maquina de hacer pajaros',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 445',
            'job' => 'Developer'
        ]);

        $user = User::where('ci', '25824044')->first();

        $this->assertTrue(!$user->hasRole('ambassador'));

        $this->assertDatabaseMissing('ambassadors', [
            'user_id' => $user->id,
            'instagram_account' => 'john_doe',
            'another_social_account' => 'facebook'
        ]);

    }
    public function test_the_user_must_no_be_registered_as_ambassador()
    {

        $this->actingAs($this->createAdminUser());

        // $this->withoutExceptionHandling();

        $user = factory(User::class)->create()->assignRole(['keyworker', 'ambassador']);

        factory(Ambassador::class)->create([
            'user_id' => $user->id
        ]);

        $this
        ->from(route('admin.ambassadors.create'))
        ->post(route('admin.ambassadors.store'), [
            'is_keyworker' => 'on',
            'user_id' => $user->id,
            'organization' => 'Promar TV',
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
        ])
        ->assertRedirect(route('admin.ambassadors.create'))
        ->assertSessionHasErrors('user_id', 'El usuario ya es un embajador');

        $this->assertDatabaseMissing('ambassadors', [
            'user_id' => $user->id,
            'ambassador_code' => 'gaby-2',
            'instagram_account' => 'gaby_teran',
            'another_social_account' => null,
        ]);

    }
}
