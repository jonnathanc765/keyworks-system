<?php

namespace Tests\Feature;

use App\Ambassador;
use App\Organization;
use App\Plan;
use App\PlanUser;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use App\Traits\TDDUtilities;
use Carbon\Carbon;
use Tests\TestCase;

class KeyworkerModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_shows_a_keyworkers_list()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        factory(User::class)->create([
            'name' => 'Johana Doe'
        ])->assignRole('keyworker');

        $this->get(route('admin.keyworkers.index'))
        ->assertStatus(200)
        ->assertViewHas('keyworkers')
        ->assertViewIs('admin.keyworkers.index')
        ->assertSee('Lista de keyworkers')
        ->assertSee('John Doe')
        ->assertSee('Google')
        ->assertSee('Fullstack Developer')
        ->assertSee('Ver')
        ->assertSee('Johana Doe');

    }

    public function test_it_shows_a_keyworker_form()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        factory(Plan::class)->create([
            'name' => 'Plan F10'
        ]);

        factory(Plan::class)->create([
            'name' => 'Plan Ctrl'
        ]);

        factory(Organization::class)->create([
            'name' => 'La máquina de hacer pajaros'
        ]);

        factory(Organization::class)->create([
            'name' => 'Syncup Marketing'
        ]);

        $this->get(route('admin.keyworkers.create'))
        ->assertStatus(200)
        ->assertViewIs('admin.keyworkers.create')
        ->assertViewHas('plans')
        ->assertViewHas('organizations')
        ->assertSee('Plan F10')
        ->assertSee('Plan Ctrl')
        ->assertSee('Registrar nuevo keyworker')
        ->assertSee('name="name"')
        ->assertSee('ci')
        ->assertSee('phone')
        ->assertSee('email')
        ->assertSee('organization')
        ->assertSee('La máquina de hacer pajaros')
        ->assertSee('Syncup Marketing')
        ->assertSee('address')
        ->assertSee('plan')
        ->assertSee('deffer_code')
        ;
    }
    public function test_it_creates_a_keyworker()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $plan = factory(Plan::class)->create([
            'name' => 'Plan F10',
            'description' => 'el mejor plan de la vida',
            'price' => 10,
            'days' => 15,
            'keyworker_charge' => 5,
            'ambassador_charge' => 5,
            'is_monthly' => false
        ]);

        $user = User::all()->first();

        factory(Ambassador::class)->create([
            'user_id' => $user->id,
            'ambassador_code' => 'john-10',
            'accumulated_amount' => 0
        ]);

        factory(Organization::class)->create([
            'name' => 'Google'
        ]);

        $this->post(route('admin.keyworkers.store'), [
            'name' => 'John Doe',
            'ci' => '25824050',
            'phone' => '04163559590',
            'job' => 'DevOps',
            'organization' => 'Google',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 465',
            'deffer_code' => 'john-10',
            'plan_id' => $plan->id
        ])
        ->assertRedirect(route('admin.keyworkers.index'))
        ->assertSessionHas('success', 'Registrado exitosamente');

        $this->assertDatabaseHas('users', [
            'name' => 'John Doe',
            'ci' => '25824050',
            'phone' => '04163559590',
            'job' => 'DevOps',
            'organization' => 'Google',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 465',
            'deffer_code' => 'john-10'
        ]);

        $user = User::where('ci', '25824050')->first();

        $this->assertDatabaseHas('plan_users', [
            'user_id' => $user->id,
            'plan_name' => 'Plan F10',
            'notes' => 'el mejor plan de la vida',
            'amount' => 5,
            'days' => 15,
            'is_monthly' => false
        ]);

        $this->assertDatabaseHas('ambassadors' , [
            'ambassador_code' => 'john-10',
            'accumulated_amount' => 5
        ]);
    }

    public function test_it_shows_keyworker_details()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'John Doe',
            'ci' => '25824050',
            'phone' => '04163559590',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 465',
            'deffer_code' => 'john-10'
        ];

        $user = factory(User::class)->create($data)->assignRole('keyworker');

        factory(PlanUser::class)->create([
            'user_id' => $user->id,
        ]);

        $this->get(route('admin.keyworkers.show', $user))
        ->assertStatus(200)
        ->assertViewHas('organizations')
        ->assertSee('John Doe')
        ->assertSee('25824050')
        ->assertSee('04163559590')
        ->assertSee('john@gmail.com')
        ->assertSee('1600 rivet point 465')
        ->assertSee('john-10');
    }

    public function test_it_updates_a_keyworker()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'John Doe',
            'ci' => '25824050',
            'phone' => '04163559590',
            'email' => 'john@gmail.com',
            'job' => 'DevOps',
            'address' => '1600 rivet point 465',
            'deffer_code' => 'john-10'
        ];

        $user = factory(User::class)->create($data);

        $this
        ->from(route('admin.keyworkers.show', $user))
        ->put(route('admin.keyworkers.update', $user), [
            'name' => 'María Doe',
            'ci' => '25824048',
            'phone' => '04169586842',
            'job' => 'Telemarketing',
            'email' => 'jonnas@gmail.com',
            'address' => '1600 rivet point 250'
        ])
        ->assertRedirect(route('admin.keyworkers.show' ,$user))
        ->assertSessionHas('success', 'Modificado exitosamente');

        $this->assertDatabaseHas('users', [
            'name' => 'María Doe',
            'ci' => '25824048',
            'phone' => '04169586842',
            'job' => 'Telemarketing',
            'email' => 'jonnas@gmail.com',
            'address' => '1600 rivet point 250'
        ]);

    }
    public function test_it_shows_the_plan_users_details()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create()->assignRole('keyworker');

        factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'notes' => 'Plan de nueve dias con hidratación y cafe',
            'plan_name' => 'Control',
            'expiration_date' => Carbon::now()->add(5, 'days'),
            'amount' => 50,
            'is_monthly' => true,
            'days' => null
        ]);

        $this->get(route('admin.keyworkers.show', $user))
        ->assertStatus(200)
        ->assertSee('Control')
        ->assertSee('Plan de nueve dias con hidratación y cafe')
        ->assertSee('5')
        ->assertSee('Si')
        ;
    }
    public function test_it_show_last_plan_when_is_expirated()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create()->assignRole('keyworker');

        factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'notes' => 'Plan de nueve dias con hidratación y cafe',
            'plan_name' => 'Control',
            'expiration_date' => Carbon::now()->sub(5, 'days'),
            'amount' => 50,
            'is_monthly' => true,
            'days' => null
        ]);

        $this->get(route('admin.keyworkers.show', $user))
        ->assertStatus(200)
        ->assertSee('Control')
        ->assertSee('Plan de nueve dias con hidratación y cafe')
        ->assertSee('Si')
        ->assertSee('Plan vencido')
        ;
    }
    public function test_it_deletes_a_keyworker()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'John Doe',
            'ci' => '25824050',
            'phone' => '04163559590',
            'email' => 'john@gmail.com',
            'address' => '1600 rivet point 465',
            'deffer_code' => 'john-10'
        ];

        $keyworker = factory(User::class)->create($data)->assignRole('keyworker');

        $this
        ->from(route('admin.keyworkers.index'))
        ->delete(route('admin.keyworkers.destroy', $keyworker))
        ->assertRedirect(route('admin.keyworkers.index'))
        ->assertSessionHas('success', 'Borrado exitosamente');

        $keyworker = User::where('ci', '25824050')->first();

        $this->assertTrue(!$keyworker->hasRole('keyworker'));
    }
    public function test_it_shows_plan_renew_page()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        factory(Plan::class)->create([
            'name' => 'Plan Control',
            'description' => 'plan de mensual ideal para los keyworkers',
        ]);
        $this->assertTrue(true);

    }
    public function test_it_shows_current_month_amount()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'organization' => 'Google',
            'job' => 'Fullstack Developer',
        ])->assignRole('keyworker');

        $date = Carbon::now();
        $oneMoreMonth = Carbon::now()->add(1, 'months');

        factory(PlanUser::class)->create([
            'user_id' => $user->id,
            'plan_name' => 'Plan Control',
            'notes' => 'Plan de 1 mes con 3 freepasses',
            'expiration_date' => $oneMoreMonth,
            'amount' => 60,
            'is_monthly' => true,
        ]);


        $this->get(route('admin.keyworkers.index'))
        ->assertStatus(200)
        ->assertSee('John Doe')
        ->assertSee('Plan Control')
        ->assertSee('pendiente')
        ->assertSee('$ 60')
        ->assertSee($date->format('F'))
        ;

    }
    public function test_start_date_is_registered_when_is_specified()

        {
            $this->actingAs($this->createAdminUser());
            $this->withoutExceptionHandling();

            $plan = factory(Plan::class)->create([
                'name' => 'Plan F10',
                'description' => 'el mejor plan de la vida',
                'price' => 10,
                'days' => 15,
                'keyworker_charge' => 0,
                'ambassador_charge' => 0,
                'is_monthly' => false
            ]);

            factory(Organization::class)->create([
                'name' => 'Google'
            ]);

            $this
            ->from(route('admin.keyworkers.create'))
            ->post(route('admin.keyworkers.store'), [
                'name' => 'John Doe',
                'ci' => '25824050',
                'phone' => '04163559590',
                'job' => 'DevOps',
                'organization' => 'Google',
                'email' => 'john@gmail.com',
                'address' => '1600 rivet point 465',
                'plan_id' => $plan->id,
                'start_date' => '2020-02-02'
            ])
            ->assertRedirect(route('admin.keyworkers.index'))
            ->assertSessionHas('success', 'Registrado exitosamente');

            $this->assertDatabaseHas('users', [
                'name' => 'John Doe',
                'ci' => '25824050',
                'phone' => '04163559590',
                'job' => 'DevOps',
                'organization' => 'Google',
                'email' => 'john@gmail.com',
                'address' => '1600 rivet point 465',
            ]);

            $user = User::where('ci', '25824050')->first();

            $this->assertDatabaseHas('plan_users', [
                'user_id' => $user->id,
                'plan_name' => 'Plan F10',
                'notes' => 'el mejor plan de la vida',
                'amount' => 10,
                'days' => 15,
                'expiration_date' => '2020-03-02',
                'is_monthly' => false,
                'created_at' => Carbon::parse('2020-02-02')
            ]);

        }

}
