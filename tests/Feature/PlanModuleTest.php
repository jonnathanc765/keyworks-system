<?php

namespace Tests\Feature;

use App\Plan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Traits\TDDUtilities;

class PlanModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    public function test_it_shows_list_plans()
    {

        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        factory(Plan::class)->create([
            'name' => 'Plan F10',
            'price' => '60',
            'days' => 10
        ]);

        factory(Plan::class)->create([
            'name' => 'Control',
            'price' => '70',
            'days' => 15
        ]);

        $this->get(route('admin.plans.index'))
        ->assertStatus(200)
        ->assertViewHas('plans')
        ->assertViewIs('admin.plans.index')
        ->assertSee('Lista de planes')
        ->assertSee('Plan F10')
        ->assertSee('60')
        ->assertSee('10')
        ->assertSee('Control')
        ->assertSee('70')
        ->assertSee('15')
        ;



    }

    public function test_it_shows_the_empty_message()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $this->get(route('admin.plans.index'))
        ->assertStatus(200)
        ->assertSee('No hay planes registrados');
    }
    public function test_it_shows_the_plan_form()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $this->get(route('admin.plans.create'))
        ->assertStatus(200)
        ->assertViewIs('admin.plans.create')
        ->assertSee('Crear nuevo plan')
        ->assertSee('name')
        ->assertSee('description')
        ->assertSee('days')
        ->assertSee('price');

    }
    public function test_it_creates_a_new_plan()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Plan F10',
            'description' => 'Plan de 10 dias con hidratacion y todos los beneficios',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 0
        ];

        $this->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHas('success', 'Registrado exitosamente');

        $this->assertDatabaseHas('plans', $data);
    }
    public function test_included_freepasses_should_not_be_less_than_zero()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Plan de 10 dias con hidratacion y todos los beneficios',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => -1
        ];

        $this
        ->from(route('admin.plans.create'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.create'))
        ->assertSessionHasErrors('included_freepasses', 'El campo freepasses incluidos debe ser mayor que 0');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_included_freepasses_should_be_a_number()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Plan de 10 dias con hidratacion y todos los beneficios',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => "String"
        ];

        $this
        ->from(route('admin.plans.create'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.create'))
        ->assertSessionHasErrors('included_freepasses', 'El campo freepasses incluidos debe ser numérico');

        $this->assertDatabaseMissing('plans', $data);
    }

    public function test_the_included_freepasses_can_be_zero()
    {
        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Plan F10',
            'description' => 'Plan de 10 dias con hidratacion y todos los beneficios',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 2
        ];

        $this->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHas('success', 'Registrado exitosamente');

        $this->assertDatabaseHas('plans', $data);
    }
    public function test_it_name_required_when_creates_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => '',
            'description' => 'Plan de 10 dias con hidratacion y todos los beneficios',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('name');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_description_required_when_create_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => '',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('description');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_keyworker_charge_required_when_create_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Description',
            'keyworker_charge' => '',
            'ambassador_charge' => '5',
            'price' => '30',
            'days' => '10',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('keyworker_charge');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_ambassador_charge_required_when_create_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Description',
            'keyworker_charge' => '5',
            'ambassador_charge' => '',
            'price' => '30',
            'days' => '10',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('ambassador_charge');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_price_required_when_create_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Description',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '',
            'days' => '10',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('price');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_days_required_when_create_a_plan()
    {
        $this->actingAs($this->createAdminUser());

        $data = [
            'name' => 'Plan F10',
            'description' => 'Description',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '10',
            'days' => '',
        ];

        $this->from(route('admin.plans.index'))
        ->post(route('admin.plans.store'), $data)
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHasErrors('days');

        $this->assertDatabaseMissing('plans', $data);
    }
    public function test_it_shows_the_plan_details()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 8
        ];

        $plan = factory(Plan::class)->create($data);

        $this->get(route('admin.plans.show', $plan))
        ->assertStatus(200)
        ->assertSee('Detalles del plan')
        ->assertSee('Plan F10')
        ->assertSee('El mejor plan que existe en la vida')
        ->assertSee('25')
        ->assertSee('35')
        ->assertSee('30')
        ->assertSee('10')
        ->assertSee('8')
        ;

    }

    public function test_it_updates_the_plan()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();


        $plan = factory(Plan::class)->create([
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 5
        ]);

        $this
        ->from(route('admin.plans.show', $plan))
        ->put(route('admin.plans.update', $plan), [
            'name' => 'Plan F1',
            'description' => 'El mejor plan que existe en la vida y existirá',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '40',
            'days' => '30',
            'included_freepasses' => 3
        ])
        ->assertRedirect(route('admin.plans.show', $plan))
        ->assertSessionHas('success', 'Modificado exitosamente');

        $this->assertDatabaseHas('plans', [
            'name' => 'Plan F1',
            'description' => 'El mejor plan que existe en la vida y existirá',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '40',
            'days' => '30',
            'included_freepasses' => 3
        ]);
    }
    public function test_included_freepasses_must_be_greater_than_zero_when_plan_its_update()
    {
        $this->actingAs($this->createAdminUser());


        $plan = factory(Plan::class)->create([
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 3
        ]);

        $this
        ->from(route('admin.plans.show', $plan))
        ->put(route('admin.plans.update', $plan), [
            'name' => 'Plan F1',
            'description' => 'El mejor plan que existe en la vida y existirá',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '40',
            'days' => '30',
            'included_freepasses' => -2
        ])
        ->assertRedirect(route('admin.plans.show', $plan))
        ->assertSessionHasErrors('included_freepasses', 'El campo freepasses incluidos debe ser mayor que 0');

        $this->assertDatabaseHas('plans', [
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 3
        ]);
    }
    public function test_included_freepasses_must_be_a_number_when_its_update()
    {
        $this->actingAs($this->createAdminUser());


        $plan = factory(Plan::class)->create([
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 3
        ]);

        $this
        ->from(route('admin.plans.show', $plan))
        ->put(route('admin.plans.update', $plan), [
            'name' => 'Plan F1',
            'description' => 'El mejor plan que existe en la vida y existirá',
            'keyworker_charge' => '5',
            'ambassador_charge' => '5',
            'price' => '40',
            'days' => '30',
            'included_freepasses' => "String"
        ])
        ->assertRedirect(route('admin.plans.show', $plan))
        ->assertSessionHasErrors('included_freepasses', 'El campo freepasses debe ser numérico');

        $this->assertDatabaseHas('plans', [
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
            'included_freepasses' => 3
        ]);
    }
    public function test_it_deletes_the_plan()
    {
        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Plan F10',
            'description' => 'El mejor plan que existe en la vida',
            'keyworker_charge' => '25',
            'ambassador_charge' => '35',
            'price' => '30',
            'days' => '10',
        ];

        $plan = factory(Plan::class)->create($data);

        $this
        ->from(route('admin.plans.index'))
        ->delete(route('admin.plans.destroy', $plan))
        ->assertRedirect(route('admin.plans.index'))
        ->assertSessionHas('success', 'Borrado exitosamente');

        $this->assertDatabaseMissing('plans', $data);
    }
}
