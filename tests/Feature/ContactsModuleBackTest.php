<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactsModuleBackTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_shows_contact_list()
    {

        $this->withoutExceptionHandling();

        factory(Contact::class)->create([
            'name' => 'Testing',
            'phone' => '04163559590'
        ]);
        factory(Contact::class)->create([
            'name' => 'Testing 2',
            'phone' => '04149574892'
        ]);

        $user = factory(User::class)->create();

        $this->actingAs($user);

        $this->get(route('admin.contact.index'))
        ->assertStatus(200)
        ->assertViewIs('admin.contact.index')
        ->assertSee('Lista de contactos')
        ->assertSee('Testing')
        ->assertSee('04163559590')
        ->assertSee('Testing 2')
        ->assertSee('04149574892');

    }
}
