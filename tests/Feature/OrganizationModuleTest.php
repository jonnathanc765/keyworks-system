<?php

namespace Tests\Feature;

use App\Organization;
use App\Traits\TDDUtilities;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrganizationModuleTest extends TestCase
{
    use RefreshDatabase;
    use TDDUtilities;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_create_a_organization()
    {

        $this->actingAs($this->createAdminUser());
        $this->withoutExceptionHandling();

        $this->post(route('admin.organizations.store'), [
            'name' => 'Google Silicon Valley'
        ])
        ->assertStatus(200)
        ->assertJson([
            'id' => 1,
            'name' => 'Google Silicon Valley'
        ]);

        $this->assertDatabaseHas('organizations', [
            'name' => 'Google Silicon Valley'
        ]);

    }
    public function test_name_must_be_unique_when_create()
    {
        $this->actingAs($this->createAdminUser());

        factory(Organization::class)->create([
            'name' => 'Google Silicon Valley'
        ]);

        $this->post(route('admin.organizations.store'), [
            'name' => 'Google Silicon Valley'
        ])
        ->assertStatus(302)
        ->assertSessionHasErrors('name');

        $this->assertDatabaseMissing('organizations', [
            'id' => 2,
            'name' => 'Google Silicon Valley'
        ]);
    }
}
