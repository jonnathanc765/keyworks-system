<?php

namespace Tests\Feature;

use App\Plan;
use App\PlanUser;
use App\Traits\TDDUtilities;
use App\User;
use App\Visit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RenewModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    public function test_it_shows_a_renew_plan_form()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        factory(Plan::class)->create([
            'name' => 'Plan Control',
            'price' => 60
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => '2020-02-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this->get(route('admin.renews.create', $keyworker))
        ->assertStatus(200)
        ->assertViewIs('admin.renews.create')
        ->assertViewHas('user')
        ->assertSee('Renovar plan de Test User')
        ->assertSee('Último plan')
        ->assertSee('Plan nuevo')
        ->assertSee('Plan de testing (Vencido el 2020-02-02)')
        ->assertSee('60')
        ;


    }
    public function test_it_shows_the_no_plans_registered_message_when_renews_a_plan()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => '2020-02-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this->get(route('admin.renews.create', $keyworker))
        ->assertStatus(200)
        ->assertViewIs('admin.renews.create')
        ->assertViewHas('user')
        ->assertSee('Renovar plan de Test User')
        ->assertSee('Último plan')
        ->assertSee('No hay planes registrados')
        ->assertSee('Plan de testing (Vencido el 2020-02-02)')
        ;
    }
    public function test_the_plan_must_be_expirated_when_a_renew_is_created()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => now()->add(15, 'days'),
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this
        ->from(route('admin.keyworkers.show', $keyworker))
        ->get(route('admin.renews.create', $keyworker))
        ->assertRedirect(route('admin.keyworkers.show', $keyworker))
        ->assertSessionHas('error', 'Este keyworker aun tiene un plan activo')
        ;
    }
    public function test_current_plans_visits_must_be_completed_when_a_renew_is_created()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => null,
            'amount' => 60,
            'is_monthly' => false,
            'days' => 10,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        Visit::create([
            'plan_user_id' => $plan->id,
            'user_id' => $keyworker->id
        ]);

        $this
        ->from(route('admin.keyworkers.show', $keyworker))
        ->get(route('admin.renews.create', $keyworker))
        ->assertRedirect(route('admin.keyworkers.show', $keyworker))
        ->assertSessionHas('error', 'Este keyworker aun tiene un plan activo')
        ;
    }
    public function test_is_shows_the_renews_form_when_visits_are_complete()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => null,
            'amount' => 60,
            'is_monthly' => false,
            'days' => 1,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        Visit::create([
            'plan_user_id' => $plan->id,
            'user_id' => $keyworker->id
        ]);

        $this
        ->from(route('admin.keyworkers.show', $keyworker))
        ->get(route('admin.renews.create', $keyworker))
        ->assertStatus(200)
        ;
    }
    public function test_it_renews_a_plan()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        factory(Plan::class)->create([
            'name'          => 'Plan Control',
            'is_monthly'    => true,
            'days' => 0,
            'description'   => 'Descripcion del plan',
            'price'         => 60
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => '2020-02-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this->post(route('admin.renews.store', $keyworker) ,[
            'plan_id' => $plan->id,
            'start_date' => '02-02-2020'
        ])
        ->assertRedirect(route('admin.keyworkers.show', $keyworker))
        ->assertSessionHas('success', 'Plan renovado exitosamente')
        ;

        $this->assertDatabaseHas('plan_users',[
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan Control',
            'notes' => 'Descripcion del plan',
            'expiration_date' => '2020-03-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => false,
            'pay_method' => null,
            'paid_date' => null,
            'paid_ref' => null,
        ]);


    }
    public function test_the_plan_date_must_no_be_required_when_plan_is_renewed()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);
        $this->withoutExceptionHandling();

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        factory(Plan::class)->create([
            'name'          => 'Plan Control',
            'is_monthly'    => true,
            'days' => 0,
            'description'   => 'Descripcion del plan',
            'price'         => 60
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => '2020-02-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this->post(route('admin.renews.store', $keyworker) ,[
            'plan_id' => $plan->id,
            'start_date' => ''
        ])
        ->assertRedirect(route('admin.keyworkers.show', $keyworker))
        ->assertSessionHas('success', 'Plan renovado exitosamente')
        ;



        $this->assertDatabaseHas('plan_users',[
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan Control',
            'notes' => 'Descripcion del plan',
            'expiration_date' => now()->add(1, 'months')->format('Y-m-d'),
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => false,
            'pay_method' => null,
            'paid_date' => null,
            'paid_ref' => null,
        ]);

    }
    public function test_plan_id_must_be_required_when_the_plan_it_renewed()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        factory(Plan::class)->create([
            'name'          => 'Plan Control',
            'is_monthly'    => true,
            'days'          => 0,
            'description'   => 'Descripcion del plan',
            'price'         => 60
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => '2020-02-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this
        ->from(route('admin.renews.create', $keyworker))
        ->post(route('admin.renews.store', $keyworker) ,[
            'plan_id' => '',
            'start_date' => '02-02-2020'
        ])
        ->assertRedirect(route('admin.renews.create', $keyworker))
        ->assertSessionHasErrors('plan_id', 'el campo plan es requerido')
        ;

        $this->assertDatabaseMissing('plan_users', [
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan Control',
            'notes' => 'Descripcion del plan',
            'expiration_date' => '2020-03-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => false,
            'pay_method' => null,
            'paid_date' => null,
            'paid_ref' => null,
        ]);

    }
    public function test_the_plan_must_be_finished()
    {
        $this->withoutExceptionHandling();

        $user = $this->createAdminUser();
        $this->actingAs($user);

        $keyworker = factory(User::class)->create([
            'name' => 'Test User'
        ]);

        factory(Plan::class)->create([
            'name'          => 'Plan Control',
            'is_monthly'    => true,
            'days'          => 0,
            'description'   => 'Descripcion del plan',
            'price'         => 60
        ]);

        $plan = factory(PlanUser::class)->create([
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan de testing',
            'notes' => 'plan description',
            'expiration_date' => now()->add(5,'days'),
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => true,
            'pay_method' => 'Paid Test',
            'paid_date' => now()->sub(1, 'months'),
            'paid_ref' => 'Ref. 1232',
        ]);

        $this
        ->post(route('admin.renews.store', $keyworker) ,[
            'plan_id' => $plan->id,
            'start_date' => '02-02-2020'
        ])
        ->assertRedirect(route('admin.keyworkers.show', $keyworker))
        ->assertSessionHas('error', 'Este keyworker posee un plan activo')
        ;

        $this->assertDatabaseMissing('plan_users', [
            'user_id' => $keyworker->id,
            'plan_name' => 'Plan Control',
            'notes' => 'Descripcion del plan',
            'expiration_date' => '2020-03-02',
            'amount' => 60,
            'is_monthly' => true,
            'days' => 0,
            'is_paid' => false,
            'pay_method' => null,
            'paid_date' => null,
            'paid_ref' => null,
        ]);

    }
}
