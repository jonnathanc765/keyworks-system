<?php

namespace Tests\Feature;

use App\Traits\TDDUtilities;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Freepass;
use Carbon\Carbon;

class FreepassRedeemModulteTest extends TestCase
{
    use RefreshDatabase;
    use TDDUtilities;
    public function test_it_shows_the_freepass_redeem_form()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->createAdminUser());


        $this->get(route('admin.freepasses.redeem.create'))
            ->assertStatus(200)
            ->assertViewIs('admin.freepasses.redeem.create')
            ->assertSee('Canjear freepass')
            ->assertSee('Ingrese el codigo del freepass')
            ->assertSee('Hacer canje')
        ;


    }
    public function test_it_redeems_the_freepass()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id'   => $user->id,
            'user_type' => 'Admin',
            'code'      => '5S3SDAS',
            'status'    => 'ACTIVO',
            'name'      => null,
            'email'     => null,
            'phone'     => null
        ]);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'     => '5S3SDAS',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.success'))
        ->assertSessionHas('success', 'El freepass se ha canjeado exitosamente')
        ;
        $this->assertDatabaseHas('freepasses', [
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'code'     => '5S3SDAS',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);

    }
    public function test_it_redeems_the_freepass_with_a_physical_Code()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id'   => $user->id,
            'user_type' => 'Admin',
            'physical_code' => '2',
            'code'      => null,
            'status'    => 'ACTIVO',
            'name'      => null,
            'email'     => null,
            'phone'     => null
        ]);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.success'))
        ->assertSessionHas('success', 'El freepass se ha canjeado exitosamente')
        ;
        $this->assertDatabaseHas('freepasses', [
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'physical_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);
    }
    public function test_it_the_freepass_code_must_be_active()
    {
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id'   => $user->id,
            'user_type' => 'Admin',
            'physical_code' => '2',
            'status'    => 'CANJEADO',
            'code' => null
        ]);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.create'))
        ->assertSessionHasErrors('freepass_code', 'Este codigo de freepass se encuentra en estado CANJEADO')
        ;
        $this->assertDatabaseMissing('freepasses', [
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'physical_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);
    }
    public function test_it_the_freepass_code_must_be_exists()
    {
        $user = $this->createAdminUser();
        $this->withoutExceptionHandling();
        $this->actingAs($user);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.create'))
        ->assertSessionHasErrors('freepass_code', 'Este freepass no esta registrado')
        ;
        $this->assertDatabaseMissing('freepasses', [
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'physical_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);
    }
    public function test_it_the_freepass_a_valid_expration_date_for_redeem()
    {
        $user = $this->createAdminUser();
        $this->withoutExceptionHandling();
        $this->actingAs($user);

        $freepass = factory(Freepass::class)->create([
            'expiration_date' => now()->sub(4, 'days'),
            'code' => '232XDKG'
        ]);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'     => '232XDKG',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.create'))
        ->assertSessionHasErrors('freepass_code', 'Este freepass se encuentra expirado')
        ;
        $this->assertDatabaseMissing('freepasses', [
            'id'                => $freepass->id,
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'physical_code'     => '2',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);
    }
    public function test_the_freepass_code_can_be_lowercase()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id'   => $user->id,
            'user_type' => 'Admin',
            'physical_code' => null,
            'code'      => '8SK4MNX',
            'status'    => 'ACTIVO',
            'name'      => null,
            'email'     => null,
            'phone'     => null
        ]);

        $this
        ->from(route('admin.freepasses.redeem.create'))
        ->post(route('admin.freepasses.redeem.store'), [
            'freepass_code'              => '8sk4mnx',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590',
        ])
        ->assertRedirect(route('admin.freepasses.redeem.success'))
        ->assertSessionHas('success', 'El freepass se ha canjeado exitosamente')
        ;
        $this->assertDatabaseHas('freepasses', [
            'user_id'           => $user->id,
            'user_type'         => 'Admin',
            'code'              => '8SK4MNX',
            'name'              => 'John Doe',
            'email'             => 'john@doe.com',
            'phone'             => '04163559590'
        ]);
    }
}
