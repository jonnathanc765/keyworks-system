<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Traits\TDDUtilities;

class AdminModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    public function test_it_shows_the_index_page_for_admin()
    {
        
        $this->withoutExceptionHandling();

        $this->actingAs($this->createAdminUser());

        $this->get(route('admin.index'))
        ->assertStatus(200)
        ->assertViewIs('admin.index')
        ->assertSee('Página de administrador');

    }
}
