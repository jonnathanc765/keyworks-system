<?php

namespace Tests\Feature;

use App\Freepass;
use App\Traits\TDDUtilities;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminFreepassesModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;
    public function test_it_shows_the_freepasses_generator_form()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');


        factory(Freepass::class, 2)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
        ]);

        $this->get(route('admin.freepasses.create', [$user, 'Admin']))
            ->assertStatus(200)
            ->assertViewIs('admin.freepasses.create')
            ->assertViewHas('user_type')
            ->assertSee('Control de freepasses')
            ->assertSee('John Doe')
            ->assertSee('Cantidad de freepasses a generar')
            ->assertSee('Freepasses actualmente activos: 2')
            ->assertSee('quantity')
        ;

    }
    public function test_its_shows_the_all_freepasses_list()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SKN25S6',
            'status' => 'CANCELADO',
            'expiration_date' => Carbon::now()->add(2, 'months')->format('Y-m-d')
        ]);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SA5S6',
            'status' => 'CANJEADO',
            'expiration_date' => Carbon::now()->add(2, 'days')->format('Y-m-d')

        ]);

        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('H5HDH26A')
        ->assertSee(Carbon::now()->add(3, 'months')->format('Y-m-d'))
        ->assertSee('2SKN25S6')
        ->assertSee(Carbon::now()->add(2, 'months')->format('Y-m-d'))
        ->assertSee('Freepasses activos: 1')
        ->assertSee('2SA5S6')

        ;

    }

    public function test_it_generates_a_freepassess_when_a_number_is_given()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');

        $this
        ->from(route('admin.freepasses.create', [$user, 'Admin']))
        ->post(route('admin.freepasses.store', [$user, 'Admin']), [
            'quantity' => 2
        ])
        ->assertRedirect(route('freepasses.index', [$user, 'Admin']))
        ->assertSessionHas('success', 'Freepasses generados exitosamente');


        $this->assertDatabaseHas('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);

    }
    public function test_it_register_a_physical_freepass()
    {

        $this->withoutExceptionHandling();
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');

        $this
        ->from(route('admin.freepasses.create', [$user, 'Admin']))
        ->post(route('admin.freepasses.store', [$user, 'Admin']), [
            'is_physical' => "on",
            'freepasses_number' => '1,8,148',
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'phone' => '04163559590'
        ])
        ->assertRedirect(route('freepasses.index', [$user, 'Admin']))
        ->assertSessionHas('success', 'Freepasses generados exitosamente');


        $this->assertDatabaseHas('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d'),
            'physical_code' => 1,
            'code' => null,
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'phone' => '04163559590'
        ]);

        $this->assertDatabaseHas('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d'),
            'physical_code' => 8,
            'code' => null,
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'phone' => '04163559590'
        ]);

        $this->assertDatabaseHas('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d'),
            'physical_code' => 148,
            'code' => null,
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'phone' => '04163559590'
        ]);

    }
    public function test_the_physical_code_should_no_be_active_in_database()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'physical_code' => 2,
            'code' => null
        ]);

        $this
        ->from(route('admin.freepasses.create', [$user, 'Admin']))
        ->post(route('admin.freepasses.store', [$user, 'Admin']), [
            'is_physical' => "on",
            'freepasses_number' => '1,2,5',
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'phone' => '04163559590'
        ])
        ->assertRedirect(route('admin.freepasses.create', [$user, 'Admin']))
        ->assertSessionHasErrors('freepasses_number', 'Ya existe un freepass activo de código 2');
    }
    public function test_quantity_must_be_greater_than_one_when_freepass_its_generated()
    {
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');

        $this
        ->from(route('admin.freepasses.create', [$user, 'Admin']))
        ->post(route('admin.freepasses.store', [$user, 'Admin']), [
            'quantity' => -1
        ])
        ->assertRedirect(route('admin.freepasses.create', [$user, 'Admin']))
        ->assertSessionHasErrors('quantity', 'el campo cantidad debe ser mayor que 1');


        $this->assertDatabaseMissing('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
    }
    public function test_quantity_must_be_a_number_when_generate_a_freepass()
    {
        $this->actingAs($this->createAdminUser());

        $user = factory(User::class)->create([
            'name' => 'John Doe'
        ])->assignRole('admin');

        $this
        ->from(route('admin.freepasses.create', [$user, 'Admin']))
        ->post(route('admin.freepasses.store', [$user, 'Admin']), [
            'quantity' => "String"
        ])
        ->assertRedirect(route('admin.freepasses.create', [$user, 'Admin']))
        ->assertSessionHasErrors('quantity', 'el campo cantidad debe ser un número');


        $this->assertDatabaseMissing('freepasses', [
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
    }
    public function test_it_shows_active_freepasses()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SKN25S6',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->add(2, 'months')->format('Y-m-d')
        ]);

        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('H5HDH26A')
        ->assertSee(Carbon::now()->add(3, 'months')->format('Y-m-d'))
        ->assertSee('2SKN25S6')
        ->assertSee(Carbon::now()->add(2, 'months')->format('Y-m-d'))
        ->assertSee('Freepasses activos: 2')

        ;
    }
    public function test_it_shows_empty_message_for_active_Freepasses()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);


        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('No tiene freepasses activos')

        ->assertSee('Freepasses activos: 0')

        ;
    }
    public function test_it_shows_redeemeds_freepasses_list()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'CANJEADO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SKN25S6',
            'status' => 'CANJEADO',
            'expiration_date' => Carbon::now()->add(2, 'months')->format('Y-m-d')
        ]);

        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('H5HDH26A')
        ->assertSee(Carbon::now()->add(3, 'months')->format('Y-m-d'))
        ->assertSee('2SKN25S6')
        ->assertSee(Carbon::now()->add(2, 'months')->format('Y-m-d'))

        ;
    }
    public function test_it_shows_empty_message_for_redeemeds_freepasses()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);


        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('No tiene freepasses canjeados')

        ->assertSee('Freepasses activos: 0')

        ;
    }
    public function test_it_shows_cancelled_freepasses_list()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'CANCELADO',
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);
        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SKN25S6',
            'status' => 'CANCELADO',
            'expiration_date' => Carbon::now()->add(2, 'months')->format('Y-m-d')
        ]);

        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('H5HDH26A')
        ->assertSee(Carbon::now()->add(3, 'months')->format('Y-m-d'))
        ->assertSee('2SKN25S6')
        ->assertSee(Carbon::now()->add(2, 'months')->format('Y-m-d'))

        ;
    }
    public function test_it_shows_empty_message_for_cancelled_freepasses()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);


        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('No tiene freepasses cancelados')

        ->assertSee('Freepasses activos: 0')

        ;
    }
    public function test_it_shows_expired_freepasses_list()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->sub(3, 'months')->format('Y-m-d')
        ]);
        factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => '2SKN25S6',
            'status' => 'ACTIVO',
            'expiration_date' => Carbon::now()->sub(2, 'months')->format('Y-m-d')
        ]);

        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('H5HDH26A')
        ->assertSee('VENCIDO')
        ->assertSee(Carbon::now()->sub(3, 'months')->format('Y-m-d'))
        ->assertSee('2SKN25S6')
        ->assertSee(Carbon::now()->sub(2, 'months')->format('Y-m-d'))

        ;
    }
    public function test_it_shows_empty_message_for_expired_freepasses()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);


        $this->get(route('freepasses.index', [$user, 'Admin']))
        ->assertStatus(200)
        ->assertViewIs('freepasses.index')
        ->assertSee('Lista de freepasses')
        ->assertSee('No tiene freepasses vencidos')

        ->assertSee('Freepasses activos: 0')

        ;
    }
    public function test_it_sending_freepass_email()
    {

        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);

        $freepass = factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'ACTIVO',
            'email' => null,
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);


        $this
        ->from(route('freepasses.index', [$user, 'Admin']))
        ->post(route('freepasses.send', [$user, 'Admin']), [
            'email' => 'john@doe.com',
            'freepass_id' => $freepass->id
        ])
        ->assertRedirect(route('freepasses.index', [$user, 'Admin']))
        ->assertSessionHas('success', "Freepass enviado exitosamente a john@doe.com")
        ;

        $this->assertDatabaseHas('freepasses', [
            'code' => 'H5HDH26A',
            'email' => 'john@doe.com'
        ]);
    }
    public function test_it_cancel_a_freepass()
    {
        $this->withoutExceptionHandling();
        $user = $this->createAdminUser();
        $this->actingAs($user);


        $freepass = factory(Freepass::class)->create([
            'user_id' => $user->id,
            'user_type' => 'Admin',
            'code' => 'H5HDH26A',
            'status' => 'ACTIVO',
            'email' => null,
            'expiration_date' => Carbon::now()->add(3, 'months')->format('Y-m-d')
        ]);


        $this
        ->from(route('freepasses.index', [$user, 'Admin']))
        ->post(route('freepasses.cancel', [$user, 'Admin', $freepass]))
        ->assertRedirect(route('freepasses.index', [$user, 'Admin']))
        ->assertSessionHas('success', "El freepass se ha cancelado exitosamente")
        ;

        $this->assertDatabaseHas('freepasses', [
            'code' => 'H5HDH26A',
            'status' => 'CANCELADO'
        ]);


    }

}
