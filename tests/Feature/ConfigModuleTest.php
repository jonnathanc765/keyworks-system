<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Traits\TDDUtilities;
use App\User;

class ConfigModuleTest extends TestCase
{
    use TDDUtilities;
    use RefreshDatabase;

    public function test_it_shows_the_config_page()
    {

        $this->actingAs($this->createAdminUser());

        $this->withoutExceptionHandling();

        $this->get(route('config.index'))
        ->assertStatus(200)
        ->assertViewIs('config.index')
        ->assertSee('Configuración')
        ->assertSee('Cédula')
        ->assertSee('Nombre')
        ->assertSee('Correo')
        ->assertSee('Teléfono')
        ->assertSee('Dirección')
        ->assertSee('Ocupación')
        ->assertSee('Contraseña')
        ->assertSee('Repita la contraseña')
        ;

    }
    public function test_it_update_user_logged()
    {
        $this->withoutExceptionHandling();
        $this->createAdminUser();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'email' => 'john@gmail.com',
            'phone' => '322765098',
            'organization' => 'Google',
            'address' => 'Sillicon Valey',
            'job' => 'DevOps',
        ]);

        $this->actingAs($user);
        $this
        ->from(route('config.index'))
        ->put(route('config.update'),[
            'name' => 'Johana Doe',
            'organization' => 'Yahoo',
            'job' => 'DevOps Admin',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ])
        ->assertRedirect(route('config.index'))
        ->assertSessionHas('success', 'Modificado exitosamente');

        $this->assertDatabaseHas('users', [
            'name' => 'Johana Doe',
            'organization' => 'Yahoo',
            'job' => 'DevOps Admin',
        ]);

        $this->assertCredentials([
            'email' => 'john@gmail.com',
            'password' => 'newpassword'
        ]);
    }
    public function test_password_must_be_the_same()
    {
        $this->createAdminUser();

        $user = factory(User::class)->create([
            'name' => 'John Doe',
            'email' => 'john@gmail.com',
            'phone' => '322765098',
            'organization' => 'Google',
            'address' => 'Sillicon Valey',
            'job' => 'DevOps',
        ]);

        $this->actingAs($user);
        $this
        ->from(route('config.index'))
        ->put(route('config.update'),[
            'name' => 'Johana Doe',
            'organization' => 'Yahoo',
            'job' => 'DevOps Admin',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword2'
        ])
        ->assertRedirect(route('config.index'))
        ->assertSessionHasErrors('password');


        $this->assertCredentials([
            'email' => 'john@gmail.com',
            'password' => 'password'
        ]);
    }
    public function test_the_password_should_not_required()
    {
        $this->withoutExceptionHandling();
        $this->createAdminUser();

        $user = factory(User::class)->create([
            'name'          => 'John Doe',
            'email'         => 'john@gmail.com',
            'phone'         => '322765098',
            'organization'  => 'Google',
            'address'       => 'Sillicon Valey',
            'job'           => 'DevOps',
        ]);

        $this->actingAs($user);
        $this
        ->from(route('config.index'))
        ->put(route('config.update'),[
            'name'          => 'Johana Doe',
            'organization'  => 'Yahoo',
            'job'           => 'DevOps Admin',
        ])
        ->assertRedirect(route('config.index'))
        ->assertSessionHas('success', 'Modificado exitosamente');

        $this->assertDatabaseHas('users', [
            'name'          => 'Johana Doe',
            'organization'  => 'Yahoo',
            'job'           => 'DevOps Admin',
        ]);


    }
}
