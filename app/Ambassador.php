<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambassador extends Model
{
    protected $fillable = [ 'user_id', 'ambassador_code', 'active_freepasses', 'instagram_account', 'another_social_account', 'accumulated_amount', 'paid_amount'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
