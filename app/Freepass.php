<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Freepass extends Model
{
    protected $fillable = ["code", "status", "name", "email", "phone", "expiration_date", "user_id", "user_type", 'physical_code'];
}
