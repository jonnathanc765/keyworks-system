<?php

namespace App\Traits;

use App\User;
use Spatie\Permission\Models\Role;

trait TDDUtilities
{

    public function createAdminUser()
    {
        $role = Role::create(['name' => 'super-admin']);
        $role = Role::create(['name' => 'ambassador']);
        $role = Role::create(['name' => 'keyworker']);
        $role = Role::create(['name' => 'admin']);
        return factory(User::class)->create(['name' => 'Admin User'])->assignRole('super-admin');
    }

}
