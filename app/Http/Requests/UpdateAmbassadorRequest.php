<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAmbassadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:191',
            'ci' => 'numeric|nullable',
            'phone' => 'numeric|required',
            'email' => 'required|email|max:191',
            'address' => 'string|nullable|max:191',
            'job' => 'string|nullable|max:191',
            'deffer_code' => 'nullable|max:191',
            'organization' => 'string|nullable|max:191',
            'instagram_account' => 'string|nullable|max:191',
            'another_social_account' => 'string|nullable|max:191'
        ];
    }
}
