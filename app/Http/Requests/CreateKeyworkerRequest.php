<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateKeyworkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required_without:is_ambassador|string|nullable|min:3|max:191',
            'is_ambassador' => 'nullable',
            'user_id'       => 'required_if:is_ambassador,on|exists:users,id',
            'ci'            => 'nullable|numeric|unique:users,ci',
            'phone'         => 'numeric|nullable',
            'email'         => 'email|string|nullable|unique:users,email|max:191',
            'address'       => 'required|string|max:191|min:3',
            'organization'  => 'string|nullable|exists:organizations,name|max:191',
            'job'           => 'string|nullable|max:191',
            'deffer_code'   => 'string|nullable|exists:ambassadors,ambassador_code',
            'plan_id'       => 'exists:plans,id',
            'date'          => 'date|nullable'
        ];
    }
}
