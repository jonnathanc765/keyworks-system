<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateVisitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'user_id' => 'exists:users,id|required',
            'days' => 'integer|min:1|max:30|numeric',
            'date' => 'date|nullable',
            'description' => 'string|nullable|min:3|max:191'
        ];

        return $rules;
    }
}
