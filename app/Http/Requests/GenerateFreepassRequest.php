<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenerateFreepassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required_without:is_physical|nullable|integer|numeric|min:1|max:20',
            'freepasses_number' => 'required_if:is_physical,on',
            'name' => 'required_if:is_physical,on|string|nullable',
            'email' => 'required_if:is_physical,on|email|nullable',
            'phone' => 'required_if:is_physical,on|numeric|nullable',
        ];
    }
}
