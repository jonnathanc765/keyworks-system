<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateAmbassadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|min:2|required_without_all:is_keyworker|max:191',
            'phone' => 'numeric|nullable',
            'organization' => 'string|nullable|max:191',
            'instagram_account' => 'string|nullable|max:191',
            'another_social_account' => 'string|nullable|max:191',
            'is_keyworker' => 'nullable',
            'user_id' => 'required_if:is_keyworker,on|exists:users,id'
        ];
    }

}
