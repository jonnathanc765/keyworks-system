<?php

namespace App\Http\Requests;

use App\Freepass;
use Illuminate\Foundation\Http\FormRequest;

class RedeemFreepassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'freepass_code' => 'string|required|max:20',
            'name' => 'nullable|min:3|max:191',
            'email' => 'nullable|email|min:3|max:191',
            'phone' => 'nullable|numeric'
        ];
    }
}
