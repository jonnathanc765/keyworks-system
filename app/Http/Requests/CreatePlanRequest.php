<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:191',
            'description' => 'required|string|min:3|max:1000',
            'keyworker_charge' => 'required|numeric|min:0',
            'ambassador_charge' => 'required|numeric|min:0',
            'price' => 'numeric|required|min:1',
            'days' => 'integer|nullable|required_without:is_monthly|min:1',
            'is_monthly' => 'nullable',
            'included_freepasses' => 'required|integer|numeric|min:0|max:20'
        ];
    }
}
