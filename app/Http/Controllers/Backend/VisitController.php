<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVisitRequest;
use App\PlanUser;
use App\User;
use App\Visit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user = null)
    {
        if ($user) {
            if (!$user->currentPlanAllowsVisits()) {
                abort(403, 'Prohibido');
            }
        } else {

            $users = User::role('keyworker')->get()->filter(function ($user)
            {
                return $user->currentPlanAllowsVisits();
            });

            return view('admin.visits.create', compact('users', 'user'));
        }

        return view('admin.visits.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateVisitRequest $request)
    {
        $data = $request->validated();

        $user = User::find($request->user_id);

        $plan = $user->lastPlan();

        if ($user->id) {
            $reaming =  $plan->days - $plan->visits->count();
            if ($request->days > $reaming) {
                return back()->withErrors(['days' => "Ya no tiene suficiente visitas disponibles, solo quedan $request->days"])->withInput($request->input());
            }
        }
        $user = User::find($request->user_id);

        if ($request->date) {
            $date = Carbon::parse($request->date)->format('Y-m-d');
        } else {
            $date = now()->format('Y-m-d');
        }

        DB::beginTransaction();
        try {
            for ($i = 0 ; $i < $request->days ; $i++ ) {
                Visit::create([
                    'plan_user_id' => $plan->id,
                    'description' => $request->description,
                    'created_at' => $date
                ]);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th);
        }
        return redirect()->route('admin.keyworkers.show' , $user)->withSuccess('Visitas registradas exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PlanUser $planUser)
    {
        $visits = $planUser->visits;
        return view('admin.visits.show', compact('visits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visit $visit)
    {
        $user = $visit->planUser->user;
        if ($user->lastPlan()->id != $visit->planUser->id) {
            return back()->withError('No se puede borrar, ya este usuario tiene otro plan vigente');
        }
        $visit->delete();
        return back()->withSuccess('Visita borrada exitosamente');
    }
}
