<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3|unique:organizations,name'
        ]);

        $organization = Organization::create([
            'name' => $request->name
        ]);

        return response()->json([
            'name' => $organization->name,
            'id' => $organization->id
        ]);
    }
}
