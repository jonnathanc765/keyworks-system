<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\RenewPlanRequest;
use App\Plan;
use App\PlanUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RenewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $lastPlan = $user->lastPlan();

        if (!$lastPlan->is_finished()) {
            return back()->withError('Este keyworker aun tiene un plan activo');
        }

        $plans = Plan::all();

        return view('admin.renews.create', compact('user', 'lastPlan', 'plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RenewPlanRequest $request, User $user)
    {
        $data = $request->validated();

            // dd($user->lastPlan());

        if (!$user->lastPlan()->is_finished()) {
            return redirect()->route('admin.keyworkers.show', $user)->withError('Este keyworker posee un plan activo');
        }

        $plan = Plan::find($data['plan_id']);

        if ($request->start_date) {
            $date = Carbon::parse($request->start_date)->add(1, 'month')->format('Y-m-d');
        } else {
            $date = now()->add(1, 'month')->format('Y-m-d');
        }

        PlanUser::create([
            'user_id' => $user->id,
            'plan_name' => $plan->name,
            'notes' => $plan->description,
            'expiration_date' => $date,
            'amount' => $plan->price,
            'is_monthly' => $plan->is_monthly,
            'days' => $plan->days,
        ]);

        return redirect()->route('admin.keyworkers.show', $user)->withSuccess('Plan renovado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
