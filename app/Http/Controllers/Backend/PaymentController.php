<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterPaymentRequest;
use App\PlanUser;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PlanUser $planUser)
    {
        if ($planUser->is_paid) {
            return redirect()
            ->route('admin.keyworkers.show', $planUser->user)
            ->withError('No puedes acceder a esta página');
        }
        return view('admin.payments.create', compact('planUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterPaymentRequest $request, PlanUser $planUser)
    {

        if ($planUser->is_paid) {
            return redirect()
            ->route('admin.keyworkers.show', $planUser->user)
            ->withError('No puedes acceder a esta página');
        }

        $data = $request->validated();

        if ($data['paid_date'] == null) {
            $data['paid_date'] = Carbon::now()->format('Y-m-d');
        }

        $data['is_paid'] = true;

        $planUser->update($data);

        return redirect()->route('admin.keyworkers.show', $planUser->user)->withSuccess('Pago registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlanUser  $planUser
     * @return \Illuminate\Http\Response
     */
    public function show(PlanUser $planUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlanUser  $planUser
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanUser $planUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlanUser  $planUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlanUser $planUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlanUser  $planUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanUser $planUser)
    {
        //
    }
}
