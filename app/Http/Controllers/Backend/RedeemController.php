<?php

namespace App\Http\Controllers\Backend;

use App\Freepass;
use App\Http\Controllers\Controller;
use App\Http\Requests\RedeemFreepassRequest;
use Illuminate\Http\Request;

class RedeemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.freepasses.redeem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RedeemFreepassRequest $request)
    {

        $data = $request->validated();

        $data['freepass_code'] = strtoupper($data['freepass_code']);

        $freepasses = Freepass::where('code', $data['freepass_code'])->orWhere('physical_code', $data['freepass_code'])->get();

        if ($freepasses->count() == 0) {
            return back()->withErrors([
                'freepass_code' => "Este freepass no esta registrado"
            ])
            ->withInput($request->input());
        }

        $validFreepass = $freepasses->filter(function ($freepass)
        {
            return $freepass->status == "ACTIVO";
        });

        if ($validFreepass->count() == 0) {
            return back()->withErrors([
                'freepass_code' => "Este codigo de freepass se encuentra en estado " . $freepasses->first()->status
            ])
            ->withInput($request->input());
        }

        if ($validFreepass->first()->expiration_date < now()) {
            return back()->withErrors([
                'freepass_code' => "Este freepass se encuentra expirado"
            ])
            ->withInput($request->input());
        }

        $validFreepass->first()->update([
            'status' => "CANJEADO",
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
        ]);

        return redirect()
            ->route('admin.freepasses.redeem.success')
            ->withSuccess('El freepass se ha canjeado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function success ()
    {
        return view('admin.freepasses.redeem.success');
    }
}
