<?php

namespace App\Http\Controllers\Backend;

use App\Ambassador;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAmbassadorRequest;
use App\Http\Requests\UpdateAmbassadorRequest;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AmbassadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ambassadors = User::role('ambassador')->orderBy('id', 'DESC')->get();

        return view('admin.ambassadors.index', compact('ambassadors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::role('keyworker')->get();
        return view('admin.ambassadors.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAmbassadorRequest $request)
    {
        $data = $request->validated();

        if ($request->is_keyworker) {

            $validator = Validator::make($request->all(), [
               'user_id' => 'unique:ambassadors,user_id'
            ], [
                'user_id.unique' => 'El usuario ya es un embajador'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $user = User::find($request->user_id);

        } else {
            $user = User::create([
                'organization' => $data['organization'],
                'name' => $data['name'],
                'phone' => $data['phone'],
            ]);
        }

        $user->assignRole('ambassador');

        $ambassador = Ambassador::create([
            'user_id' => $user->id,
            'instagram_account' => $request->instagram_account,
            'another_social_account' => $request->another_social_account,
            'phone' => $request->phone
        ]);

        $code = explode(' ', trim($user->name));
        $code = strtolower($code[0]);

        $ambassador->update([
            'ambassador_code' => $code . '-' . $ambassador->id
        ]);
        return redirect()->route('admin.ambassadors.index')->withSuccess('Registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ambassador  $ambassador
     * @return \Illuminate\Http\Response
     */
    public function show(Ambassador $ambassador)
    {
        $ambassador->load('user');
        return view('admin.ambassadors.show', compact('ambassador'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ambassador  $ambassador
     * @return \Illuminate\Http\Response
     */
    public function edit(Ambassador $ambassador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ambassador  $ambassador
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAmbassadorRequest $request, Ambassador $ambassador)
    {
        $request->validated();

        $user = $ambassador->user;

        $user->update([
            'name' => $request->name,
            'ci' => $request->ci,
            'phone' => $request->phone,
            'email' => $request->email,
            'organization' => $request->organization,
            'address' => $request->address,
            'job' => $request->job,
            'deffer_code' => strtolower($request->deffer_code)
        ]);

        $ambassador->update([
            'instagram_account' => $request->instagram_account,
            'another_social_account' => $request->another_social_account
        ]);

        return back()->withSuccess('Modificado exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ambassador  $ambassador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ambassador $ambassador)
    {
        $ambassador->user->removeRole('ambassador');
        $ambassador->delete();
        return back()->withSuccess('Borrado exitosamente');
    }
    public function code(Request $request)
    {
        $data = $request->validate([
            'deffer_code' => 'required'
        ]);

        $ambassador = Ambassador::where('ambassador_code', strtolower($data['deffer_code']))->first();

        if ($ambassador) {
            return response()->json([
                'success' => 'El embajador fue encontrado exitosamente'
            ]);
        }
        return response()->json([
            'error' => 'No fue encontrado este código de referido'
        ]);
    }
}
