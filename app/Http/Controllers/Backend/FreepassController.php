<?php

namespace App\Http\Controllers\Backend;

use App\Freepass;
use App\Http\Controllers\Controller;
use App\Http\Requests\GenerateFreepassRequest;
use App\Http\Requests\RedeemFreepassRequest;
use App\Mail\FreepassMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FreepassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user, String $user_type)
    {
        if (!$this->isRoleAvalaible($user_type)) {
            abort(404);
        }
        $all = $user->freepasses;
        $active = $user->unexpiredFreepasses()->whereStatus('ACTIVO')->get();
        $redeemed = $user->unexpiredFreepasses()->whereStatus('CANJEADO')->get();
        $expired = $user->freepasses()->where('expiration_date', '<' , Carbon::now() )->get();
        $cancelled = $user->unexpiredFreepasses()->whereStatus('CANCELADO')->get();
        return view('freepasses.index', compact('user', 'user_type', 'all', 'active', 'redeemed', 'expired', 'cancelled'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user, String $user_type)
    {
        if (!$this->isRoleAvalaible($user_type)) {
            abort(404);
        }
        $users = User::all();
        return view('admin.freepasses.create', compact('user', 'user_type', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenerateFreepassRequest $request, User $user, $user_type)
    {

        if (!$this->isRoleAvalaible($user_type)) {
            abort(404);
        }

        if ($request->is_physical) {

            $freepasses = Freepass::whereStatus('ACTIVO')->get();

            $freepasses_number = $request->freepasses_number;
            //Quitar espacios que esten mas de dos veces
            $freepasses_number = preg_replace('/( ){2,}/u', '', $freepasses_number);
            // Separar en arrays por la comas
            $freepasses_number = explode(',', $freepasses_number);

            foreach ($freepasses_number as $number) {

                $before = $number;

                $number = (int)$number;

                if ($number == 0) {
                    return back()->withErrors([
                        'freepasses_number' => "Los numeros deben ser válidos y enteros, un error con ($before)"
                    ])
                    ->withInput($request->input());
                }

                if ($freepasses->where('physical_code', $number)->first()) {

                    return back()->withErrors([
                        'freepasses_number' => "Ya existe un freepass activo de código $number"
                    ])
                    ->withInput($request->input());

                }

            }

            $expiration_date = now()->add(3, 'months')->format('Y-m-d');

            foreach ($freepasses_number as $number) {
                Freepass::create([
                    'user_id' => $user->id,
                    'user_type' => $user_type,
                    'physical_code' => $number,
                    'status' => 'ACTIVO',
                    'expiration_date' => $expiration_date,
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'email' => $request->email
                ]);
            }


        } else {

            for ($i = 0 ; $i < $request->quantity ; $i++) {
                $freepass = Freepass::create([
                    'code' => $this->generateFreepassCode(),
                    'user_id' => $user->id,
                    'user_type' => $user_type,
                    'expiration_date' => Carbon::now()->add('3', 'months')->format('Y-m-d')
                ]);
            }

        }


        return redirect()->route('freepasses.index', [$user, $user_type])->withSuccess('Freepasses generados exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Freepass  $freepass
     * @return \Illuminate\Http\Response
     */
    public function show(Freepass $freepass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Freepass  $freepass
     * @return \Illuminate\Http\Response
     */
    public function edit(Freepass $freepass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Freepass  $freepass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Freepass $freepass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Freepass  $freepass
     * @return \Illuminate\Http\Response
     */
    public function destroy(Freepass $freepass)
    {
        //
    }
    /**
     * Genera un codigo unico para el freepass
     *
     * @return string
     */
    public function generateFreepassCode()
    {

        $code = substr(md5(uniqid(mt_rand(), true)) , 0, 8);

        // call the same function if the barcode exists already
        if ($this->freepassCodeExists($code)) {
            return $this->generateProductCode();
        }

        // otherwise, it's valid and can be used
        return strtoupper($code);
    }

    /**
     * Verifica si el codigo es unico
     *
     * @return boolean
     */
    public function freepassCodeExists($code)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Freepass::whereCode($code)->exists();
    }
    public function isRoleAvalaible(String $role)
    {
        return in_array($role, ['Admin', 'Ambassador', 'Keyworker']);
    }
    public function send(Request $request, User $user, String $user_type)
    {
        if (!$this->isRoleAvalaible($user_type)) {
            abort(404);
        }

        $request->validate([
            'email' => 'required|email',
            'freepass_id' => 'required|exists:freepasses,id'
        ]);


        $freepass = Freepass::find($request->freepass_id);

        DB::beginTransaction();

        try {

            Mail::to($request->email)->send(new FreepassMail($user, $user_type, $freepass));

            $freepass->update([
                'email' => $request->email
            ]);

            DB::commit();

        } catch (\Throwable $th) {

            DB::rollBack();
            return back()->withError('No se pudo enviar el freepass, intentételo de nuevo');

        }

        return redirect()->route('freepasses.index', [ $user, 'Admin'])->withSuccess("Freepass enviado exitosamente a $request->email");
    }
    public function cancel(Request $request, User $user, String $user_type, Freepass $freepass)
    {
        if (!$this->isRoleAvalaible($user_type)) {
            abort(404);
        }

        $freepass->update([
            'status' => 'CANCELADO'
        ]);
        return back()->withSuccess('El freepass se ha cancelado exitosamente');
    }
}
