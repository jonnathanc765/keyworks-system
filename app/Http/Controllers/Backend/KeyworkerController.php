<?php

namespace App\Http\Controllers\Backend;

use App\Ambassador;
use App\Deffer;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateKeyworkerRequest;
use App\Http\Requests\UpdateKeyworkerRequest;
use App\Organization;
use App\Plan;
use App\PlanUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KeyworkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyworkers = User::role('keyworker')->orderBy('id', 'DESC')->get();

        $keyworkers->map(function ($keyworker)
        {
            $currentPlan = $keyworker->planPerMonth(Carbon::now());
            $lastPlan = $keyworker->planPerMonth(Carbon::now()->sub(1, 'months'));
            $keyworker['currentPlan'] =  $currentPlan;
            $keyworker['lastPlan'] =  $lastPlan;
        });

        return view('admin.keyworkers.index', compact('keyworkers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = Plan::all();
        $ambassadors = User::role('ambassador')->get();
        $organizations = Organization::all();

        return view('admin.keyworkers.create', compact('plans', 'users', 'ambassadors', 'organizations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateKeyworkerRequest $request)
    {
        $data = $request->validated();

        $plan = Plan::find($request->plan_id);

        $plan_price = $plan->price;

        if ($request->is_ambassador) {

            if (User::find($request->user_id)->hasRole('keyworker')) {

                return back()->withErrors([
                    'user_id' => 'Este usuario ya es un keyworker'
                ])->withInput();
            }


            $user = User::find($request->user_id);

            $user->update([
                'ci' => $request->ci,
                'email' => $request->email,
                'address' => $request->address,
                'job' => $request->job,
            ]);

        } else {
            $user = User::create($data);
        }

        $user->assignRole('keyworker');

        if ($request->deffer_code) {

            $ambassador = Ambassador::where('ambassador_code', $data['deffer_code'])->first();

            $ambassador->update([
                'accumulated_amount' => $ambassador->accumulated_amount + $plan->ambassador_charge
            ]);

            $deffer = Deffer::create([
                'user_id' => $user->id,
                'ambassador_code' => $ambassador->ambassador_code,
                'discount' => $plan->ambassador_charge,
            ]);

            $plan_price -= $plan->keyworker_charge;
        }

        if ($request->start_date) {
            $start_date = Carbon::parse($request->start_date);
            $date = Carbon::parse($request->start_date)->add(1, 'month')->format('Y-m-d');
        } else {
            $start_date = now();
            $date = Carbon::now()->add(1, 'month')->format('Y-m-d');
        }

        $plan_user = PlanUser::create([
            'user_id'           => $user->id,
            'plan_name'         => $plan->name,
            'notes'             => $plan->description,
            'amount'            => $plan_price,
            'expiration_date'   => $date,
            'days'              => $plan->days,
            'is_monthly'        => $plan->is_monthly,
            'created_at'        => $start_date
        ]);

        return redirect()->route('admin.keyworkers.index')->withSuccess('Registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        if (!$user->hasRole('keyworker')) {
            abort(404);
        }

        $monthlyPlan = $user->monthlyPlan();

        $consumablePlan = $user->consumablePlan();

        $organizations = Organization::all();

        return view('admin.keyworkers.show', compact('user', 'monthlyPlan', 'consumablePlan', 'organizations'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKeyworkerRequest $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'ci' => $request->ci,
            'phone' => $request->phone,
            'email' => $request->email,
            'job' => $request->job,
            'address' => $request->address,
            'organization' => $request->organization
        ]);

        return back()->withSuccess('Modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->removeRole('keyworker');
        foreach ($user->plans as $plan) {
            $plan->delete();
        }
        return back()->withSuccess('Borrado exitosamente');
    }
}
