<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePlanRequest;
use App\Http\Requests\UpdatePlanRequest;
use App\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::all();
        return view('admin.plans.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePlanRequest $request)
    {
        $data = $request->validated();

        $is_monthly = false;
        $days = $request->days;

        if ($request->is_monthly == "on") {
            $is_monthly = true;
            $days = 0;
        }

        Plan::create([
            'name' => $request->name,
            'description' => $request->description,
            'ambassador_charge' => $request->ambassador_charge,
            'keyworker_charge' => $request->keyworker_charge,
            'days' => $days,
            'is_monthly' => $is_monthly,
            'price' => $request->price,
            'included_freepasses' => $request->included_freepasses
        ]);

        return redirect()->route('admin.plans.index')->withSuccess('Registrado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Plan $plan)
    {
        return view('admin.plans.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Plan $plan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlanRequest $request, Plan $plan)
    {
        $data = $request->validated();


        $is_monthly = false;
        $days = $request->days;

        if ($request->is_monthly) {
            $is_monthly = true;
            $days = 0;
        }

        $plan->update([
            'name' => $request->name,
            'description' => $request->description,
            'ambassador_charge' => $request->ambassador_charge,
            'keyworker_charge' => $request->keyworker_charge,
            'days' => $days,
            'is_monthly' => $is_monthly,
            'price' => $request->price,
            'included_freepasses' => $request->included_freepasses
        ]);

        return back()->withSuccess('Modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan)
    {
        $plan->delete();
        return back()->withSuccess('Borrado exitosamente');
    }
}
