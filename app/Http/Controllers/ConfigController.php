<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateConfigRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('config.index', compact('user'));
    }
    public function update(UpdateConfigRequest $request)
    {
        $data = $request->validated();


        $user = User::find(Auth::user()->id);

        $user->update([
            'name'          => $request->name,
            'organization'  => $request->organization,
            'job'           => $request->job
        ]);

        if (isset($data['password'])) {
            $user->update([
                'password'      => bcrypt($request->password)
            ]);
        }

        return back()->withSuccess('Modificado exitosamente');
    }
}
