<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone', 'job', 'address', 'ci','deffer_code', 'organization'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ambassador()
    {
        return $this->hasOne('App\Ambassador');
    }
    public function plans()
    {
        return $this->hasMany('App\PlanUser');
    }
    public function freepasses()
    {
        return $this->hasMany('App\Freepass');
    }
    public function unexpiredFreepasses()
    {
        return $this->freepasses()->where('expiration_date', '>=' , Carbon::now());
    }
    public function lastPlan()
    {
        return $this->plans()->orderBy('id', 'DESC')->first();
    }
    public function monthlyPlans()
    {
        return $this->plans()->where('is_monthly', true);
    }
    public function consumablePlans()
    {
        return $this->plans()->where('is_monthly', false);
    }
    public function remainingDays()
    {


        $consumableRemainingDays = 0;

        $consumables = $this->consumablePlans()->get();

        if ($consumables->first() != null) {
            $remaining = $consumables->filter(function ($plan)
            {
                return !$plan->is_finished();
            });
            $consumableRemainingDays += $remaining->sum(function ($plan)
            {
                return $plan->days - $plan->visits->count();
            });
        }

        $monthly = $this->monthlyPlans()->orderBy('id', 'DESC')->first();

        if ($monthly != null) {
            if (Carbon::parse($monthly->expiration_date) >= Carbon::now()) {
                $consumableRemainingDays += Carbon::parse($monthly->expiration_date)->diffInDays(now());
            }
        }

        return $consumableRemainingDays;

    }
    public function consumablePlan()
    {
        return $this->consumablePlans()->orderBy('id', 'DESC')->first();
    }
    public function monthlyPlan()
    {
        return $this->monthlyPlans()->orderBy('id', 'DESC')->first();
    }
    public function planPerMonth($monthNumber = null)
    {
        if ($monthNumber == null) {
            $monthNumber = now()->month;
        }

        return $this->plans()->whereMonth('created_at', $monthNumber)->first();
    }

    public function freepassesType(String $user_type)
    {
        return $this->unexpiredFreepasses()->where('user_type', $user_type)->get();
    }
    public function activeFreepassesType(String $user_type)
    {
        return $this->unexpiredFreepasses()->where('user_type', $user_type)->whereStatus('ACTIVO')->get();
    }
    public function currentPlanAllowsVisits()
    {
        $currentPlan = $this->lastPlan();

        if (!$currentPlan) {
            return false;
        }

        if (!$currentPlan->is_monthly) {
            if (!$currentPlan->is_finished()) {
                return true;
            }
        }
        return false;
    }
}
