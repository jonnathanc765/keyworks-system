<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Freepass;

class FreepassMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     *  instance of the user for which the freepass was sent.
     *
     * @var User
     */
    public $user;

    /**
     *  Role of the user for which the freepass was sent.
     *
     */
    public $role;

    /**
     *  freepass instance was sent.
     */
    public $freepass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $role, Freepass $freepass)
    {
        $this->user = $user;
        $this->freepass = $freepass;
        $this->role = $role;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject('Te han enviado un freepass')
        ->view('emails.freepass')
        ->text('emails.plain.freepass');
    }
}
