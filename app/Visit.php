<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = ['plan_user_id', 'description', 'created_at'];

    public function planUser()
    {
        return $this->belongsTo('App\PlanUser');
    }
}
