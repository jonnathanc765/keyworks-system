<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['name', 'description', 'keyworker_charge', 'ambassador_charge', 'price', 'days', 'is_monthly', 'included_freepasses'];
}
