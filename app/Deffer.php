<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deffer extends Model
{
    protected $fillable = ['user_id' , 'ambassador_code', 'discount', 'plan_users_id'];
}
