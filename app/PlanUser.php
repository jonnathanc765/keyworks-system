<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PlanUser extends Model
{

    protected $fillable = ['user_id', 'plan_name', 'notes', 'amount', 'days', 'is_monthly', 'expiration_date', 'pay_method', 'paid_date', 'paid_ref', 'is_paid', 'created_at'];

    public function visits()
    {
        return $this->hasMany('App\Visit');
    }
    public function visitsQuantity()
    {
        return $this->visits->count();
    }
    public function reaming()
    {
        return $this->days - $this->visitsQuantity();
    }
    public function is_finished()
    {
        if (!$this->is_monthly) {
            return $this->days == $this->visitsQuantity();
        }
        return now() > Carbon::parse($this->expiration_date) ;
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function paid_date()
    {
        return Carbon::parse($this->paid_date)->format('d-m-Y');
    }
}
